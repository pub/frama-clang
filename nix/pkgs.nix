{ frama-c-repo ? builtins.trace "frama-clang: defaulting frama-c-repo to ${toString ../../frama-c}" ../../frama-c }:
let pkgs = import (frama-c-repo + "/nix/pkgs.nix");
    mysrc = import ./sources.nix {};
in
let
  ocamlOverlay = oself: osuper: {
    frama-clang-llvm-12 =
      oself.callPackage ./frama-clang.nix { llvmPackages=pkgs.llvmPackages_12; };
    frama-clang-llvm-13 =
      oself.callPackage ./frama-clang.nix { llvmPackages=pkgs.llvmPackages_13; };
    frama-clang-llvm-14 =
      oself.callPackage ./frama-clang.nix { llvmPackages=pkgs.llvmPackages_14; };
    frama-clang-llvm-15 =
      oself.callPackage ./frama-clang.nix { llvmPackages=pkgs.llvmPackages_15; };
    frama-clang-llvm-16 =
      oself.callPackage ./frama-clang.nix { llvmPackages=pkgs.llvmPackages_16; };
    frama-clang-llvm-17 =
      oself.callPackage ./frama-clang.nix { llvmPackages=pkgs.llvmPackages_17; };
    frama-clang-llvm-18 =
      oself.callPackage ./frama-clang.nix { llvmPackages=pkgs.llvmPackages_18; };
    frama-clang = oself.frama-clang-llvm-12 ;
    frama-clang-manual =
      oself.callPackage ./frama-clang-manual.nix { };
    frama-clang-check-versions =
      oself.callPackage ./frama-clang-check-versions.nix { };
  };
  overlay = self: super: {
    ocaml-ng = super.lib.mapAttrs (
      name: value:
        if builtins.hasAttr "overrideScope" value
        then value.overrideScope ocamlOverlay
        else value
    ) super.ocaml-ng;
    release = self.callPackage ./release.nix {
      git = pkgs.git;
      jq = pkgs.jq;
      curl = pkgs.curl;
      git-lfs = pkgs.git-lfs;
      opam = pkgs.opam;
    };
  };
  with_overlay = pkgs.appendOverlays [ overlay ];
in
with_overlay
