{ lib
, stdenv
, frama-clang
, frama-c
, texlive
} :

stdenv.mkDerivation rec {
  pname = "manual";
  version = lib.strings.replaceStrings ["~"] ["-"] frama-clang.version;

  build_dir = frama-c.build_dir + "/dir.tar";
  srcs =
    [ build_dir
      (builtins.path { path = frama-clang.src; name = "frama-clang"; }) ];

  sourceRoot = ".";

  buildInputs = [ texlive.combined.scheme-full ];

  postPatch = ''
    patchShebangs .
  '' ;

  # Keep main configuration
  configurePhase = ''
    true
  '';

  buildPhase = ''
    make -C frama-clang/doc/userman
  '';

  installPhase = ''
    mkdir -p $out
    cp frama-clang/doc/userman/main.pdf $out/frama-clang-manual-$version.pdf
  '';
}
