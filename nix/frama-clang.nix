{ lib
, mk_plugin
, cmake
, camlp5
, camlp-streams
, gnused
, llvmPackages
}:

mk_plugin {
  plugin-name = "frama-clang-on-llvm-" + llvmPackages.llvm.version;
  plugin-src = fetchGit { shallow=true ; url=./.. ; } ;
  additional-build-inputs = [
    camlp5
    camlp-streams
    cmake
    gnused
    llvmPackages.llvm.dev
    llvmPackages.clang-unwrapped.dev
  ] ;
  has-wp-proofs = true ;
  install-opam = false ;
} // {
  # Prevent CMake from doing stuff without being asked
  dontUseCmakeConfigure=true;
  # set frama-clang's version number
  version =
    lib.strings.removePrefix "version: \""
      (lib.strings.removeSuffix "\""
        (lib.lists.findFirst
          (x: lib.strings.hasPrefix "version:" x)
          ""
          (lib.strings.splitString "\n"
            (builtins.readFile ../frama-clang.opam.template))));
}
