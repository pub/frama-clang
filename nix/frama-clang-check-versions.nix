{ lib
, stdenv
, frama-clang
, frama-c
} :

stdenv.mkDerivation rec {
  pname = "check-versions";
  version = frama-clang.version;

  fclang_build_dir = builtins.path { path = frama-clang.src; name = "frama-clang"; };
  fc_build_dir = builtins.path { path = frama-c.src; name = "frama-c"; };

  srcs = [
   fclang_build_dir
   fc_build_dir
  ];

  sourceRoot = ".";

  buildInputs = [ stdenv ];

  postPatch = ''
    patchShebangs .
  '' ;

  # Keep main configuration
  configurePhase = ''
    true
  '';

  buildPhase = ''
    cmp frama-c/doc/frama-c-book.cls frama-clang/doc/userman/frama-c-book.cls
    diff -qr frama-c/doc/logos frama-clang/doc/userman/logos
    cmp frama-c/doc/anr-logo.png frama-clang/doc/userman/anr-logo.png
    cmp frama-c/doc/eu-flag.jpg frama-clang/doc/userman/eu-flag.jpg
    cmp frama-c/VERSION frama-clang/doc/userman/FC_VERSION
    cmp frama-c/VERSION_CODENAME frama-clang/doc/userman/FC_VERSION_NAME
    echo $version | cmp - frama-clang/doc/userman/FCLANG_VERSION
    test "$(grep -e '- Frama-C version' frama-clang/README.md | cut -f4 -d' '| cut -f1 -d.)" = "$(cut -f1 -d. frama-c/VERSION)"
    grep -e '- Frama-C version' frama-clang/README.md | cut -f5 -d' ' | cmp - frama-c/VERSION_CODENAME
  '';

  # you can't have empty output derivation in nix...
  installPhase = ''
    mkdir -p $out
    touch $out/.done
  '';

}
