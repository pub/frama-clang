(**************************************************************************)
(*                                                                        *)
(*  This file is part of Frama-Clang                                      *)
(*                                                                        *)
(*  Copyright (C) 2012-2025                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file LICENSE).                      *)
(*                                                                        *)
(**************************************************************************)

include
  Plugin.Register(
    struct
      let name = "Frama_Clang"
      let shortname = "fclang"
      let help = "Use clang as C++ front-end for Frama-C"
    end)


module Clang_command =
  String(
    struct
      let option_name = "-cxx-clang-command"
      let default = "framaCIRGen"
      let help =
        "use <cmd> as the parsing command. Defaults to " ^ default
      let arg_name = "cmd"
    end)

let () = Parameter_customize.no_category ()
module Clang_extra_args =
  String_list(
  struct
    let option_name = "-fclang-cpp-extra-args"
    let help =
      "pass additional options to clang. If not set, the content of \
       -cpp-extra-args (if any) is used instead"
    let arg_name = "opt"
  end)

let unmangling_functions: (string*(string->string)) Datatype.String.Hashtbl.t =
  Datatype.String.Hashtbl.create 7

let pp_unmangling_functions fmt =
  Datatype.String.Hashtbl.iter
    (fun key (help, _) -> Format.fprintf fmt "%s: %s@\n" key help)
    unmangling_functions

let () =
  Datatype.String.Hashtbl.add
    unmangling_functions "none" ("keep mangled names", Fun.id)

module Unmangling =
  String(
  struct
    let option_name = "-cxx-unmangling"
    let help =
      "how to pretty-print mangled symbols. Use `-cxx-unmangling help' to list \
       available options"
    let arg_name = "s"
    let default = "none"
  end
)

let unmangling_help () =
  feedback "Possible unmangling functions are:@\n%t" pp_unmangling_functions;
  Cmdline.nop

let unmangling_hook _ v =
  if Datatype.String.equal v "help" then
    Cmdline.run_after_exiting_stage unmangling_help

let () = Unmangling.add_set_hook unmangling_hook

let () = Unmangling.set_possible_values [ "none"; "help" ]
let add_unmangling_function key descr f =
  let l = Unmangling.get_possible_values () in
  Unmangling.set_possible_values (key :: l);
  Datatype.String.Hashtbl.add unmangling_functions key (descr,f)

let get_unmangling_function () =
  let v = Unmangling.get () in
  if v <> "help" then
    snd (Datatype.String.Hashtbl.find unmangling_functions v)
  else
    fatal
      "cannot get current unmangling function: \
       Frama-C is not supposed to run analyses if `-cxx-unmangling help' is set "

module UnmanglingFull =
  Bool(struct
             let default = false
             let option_name = "-cxx-demangling-full"
             let help= "displays identifiers with their full C++ name. \
                        DEPRECATED: use -cxx-unmangling fully-qualified instead)"
        end)

let () = Parameter_customize.set_negative_option_name ""

module UnmanglingShort =
  Bool(struct
             let default = false
             let option_name = "-cxx-demangling-short"
             let help=
               "displays identifiers with their C++ short name \
                (without qualifiers). \
                DEPRECATED: use -cxx-unmangling without-qualifier instead"
        end)

let () = Parameter_customize.set_negative_option_name ""

module UnmanglingNo =
  Bool(struct
             let default = false
             let option_name= "-cxx-keep-mangling"
             let help= "displays identifiers with their mangled name \
                        DEPRECATED: use -cxx-unmangling none instead"
           end)

let add_unmangling_option s _ new_flag =
  if new_flag then Unmangling.set s

let () =
  UnmanglingFull.add_set_hook (add_unmangling_option "fully-qualified");
  UnmanglingShort.add_set_hook (add_unmangling_option "without-qualifier");
  UnmanglingNo.add_set_hook (add_unmangling_option "none")

module ParseableOutput =
  False(struct
      let option_name = "-cxx-parseable-output"
      let help = "set up Frama-C pretty-printer to output C code that can be reparsed by Frama-C"
    end)

let parseable_output_hook _ f =
  let k = Kernel.dkey_print_attrs in
  if f then begin
      Kernel.add_debug_keys k;
      Unmangling.set "none"
    end
  else Kernel.del_debug_keys k

let () = ParseableOutput.add_set_hook parseable_output_hook

module C_std_headers =
  String(
    struct
      let default =
        (Kernel.Share.get_dir "libc" :> string)
      let option_name = "-cxx-cstdlib-path"
      let help = "<path> where to look for C standard headers \
                  (default: Frama-C libc in " ^ default ^ ")"
      let arg_name = "path"
    end)

module Cxx_std_headers =
  String(
    struct
      let default =
        (Kernel.Share.get_dir "frama-clang/libc++" :> string)
      let option_name = "-cxx-c++stdlib-path"
      let help = "<path> where to look for C++ standard headers \
                  (default: FClang libc++ in " ^ default ^ ")"
      let arg_name = "path"
    end)

module Cxx_virtual_bare_methods_in_clang =
  Bool(struct
             let default = false
             let option_name= "-cxx-vbmc"
             let help= "Asks clang to generate bare constructors and \
                        destructors in case of virtual inheritance"
           end)

let () = Parameter_customize.set_negative_option_name "-cxx-nostdinc"
module Std_include =
  Bool(
    struct
      let default = true
      let option_name = "-cxx-stdinc"
      let help = "Adds Frama-C standard headers for C and C++ \
                  in include path (default)."
    end)

let dkey_reorder = register_category "reorder"
