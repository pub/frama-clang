\chapter{Installation}

\framaclang is currently still experimental and not part of regular \FramaC releases. It must be built from source and added to a \FramaC installation.
If you're already using the \opam package manager to install \FramaC (see below),
\framaclang can be installed directly with \lstinline|opam install frama-clang|.

The remainder of this chapter gives the instructions for installing \framaclang manually.
\framaclang depends on two software packages:
\begin{itemize}
\item A current version of \FramaC itself. It is highly recommended to install \FramaC using \lstinline|opam|, as described in the installation procedures for \FramaC (\url{https://frama-c.com/download.html}). 
Version \fclangversion of \framaclang is compatible with version \framacversion of \FramaC.
\item An installation of Clang, which is available as part of LLVM,
which itself is available from \url{http://releases.llvm.org}. Note that you will need
\clang's library and its headers, not just the compiler itself.
Version \fclangversion of \framaclang is compatible with version \clangversion of \clang.
\end{itemize}

In addition, a third package is needed for compiling \framaclang,
\lstinline|camlp5| (\url{https://camlp5.github.io/}).
Once \framaclang has been installed, \lstinline|camlp5| is not required anymore.
Again, the easiest way to install
\lstinline|camlp5| itself is through \lstinline|opam|. Finally, newer versions
of OCaml have dropped the \lstinline|Genlex| and \lstinline|Streams| modules
from their standard library, so that another \lstinline|opam| package must be
installed as well, namely \lstinline|camlp-streams|.

Building and installing \framaclang has three effects:
\begin{itemize}
\item The \framaclang executable files are installed within
the \FramaC installation.
In particular, if \FramaC has been installed using \lstinline|opam|,
then the principal executable  \irg will be installed in the
\lstinline|opam| \lstinline|bin| directory.
You must be sure that this directory is on your \verb|$PATH|.
This is usually the default for standard \lstinline|opam| installations.
In doubt, you can try the command \lstinline|which framaCIRGen|
after installation to be sure that \irg will be correctly detected by your
shell.
\item The \FramaC plug-in itself is copied into the standard \FramaC plug-in
directory (as given by \lstinline|frama-c-config -print-plugin-path|), so
that it will be loaded automatically by the main \FramaC commands at each
execution.
\item Include files containing \acslpp specifications of \Cpp library functions
are copied to directory
\verb|$FRAMAC_SHARE/frama-clang/libc++|, where \verb|$FRAMAC_SHARE| is the path
given by the command \lstinline|frama-c-config -print-share-path|.
\end{itemize}
These include files are replacements for (a subset of)
the standard system include files. They should have the same definitions of
\C and \Cpp functions and classes, but
with \acslpp annotations giving their specifications. Note however that this is
still very much a work in progress, except for the headers that are imported
from the C standard library, which benefit from the specifications of the
headers provided by \FramaC itself.

The plugin can be built by hand from source using the following commands. Create a new directory to which you download and unpack the source distribution. Then \lstinline|cd| into the source directory itself (one level down) and execute:
\begin{listing-nonumber}
make
make install
\end{listing-nonumber}

By default, \framaclang will install its files under the same root
directory as \FramaC itself. In particular, if \FramaC has been
installed from \lstinline|opam|, the installation will be done under
\verb|$(opam var prefix)| directory. To install it in another directory,
you can set the \lstinline|PREFIX| environment variable before executing
\lstinline|make install|. Note that in that case, \FramaC may not be able
to load the plug-in automatically.

%\section{Current status}
%
%Currently on Ubuntu 17.10 and MacOSX (Sierra 10.12.6), \framaclang builds in this configuration:
%
%\begin{itemize}
%\item Install opam (>= 2.0)
%\item disable sandboxing by commenting out the last six lines of ~/.opam/config
%\item Install ocaml (\lstinline|opam install ocaml|)
%\item Install bwrap (\lstinline|opam install bwrap|)
%\item Install menhir, ocamlfind, camlp4, why3, alt-ergo, frama-c
%\end{itemize}
%
%To build against current development:
%
%
%To build against the released Potassium build:
%
%
%
%\begin{itemize}
%\item \framaclang branch \lstinline|cok-new-parser|, framac commit \lstinline|stable/potassium|,why3 version 0.88.3, alt-ergo version 2.0.0, llvm/clang 6.0, ocaml version 4.05.0
%
%\item \framaclang branch \lstinline|cok-new-parser|, \FramaC branch master (commit 83b5e367ff), with why3 version 1.0
%\end{itemize}

\chapter{Running the plug-in}

\section{\Cpp files}
Once installed the plugin is run automatically by \FramaC on any \Cpp
files listed on the command-line. \Cpp files are identified by their
filename suffixes. The default suffixes recognized as \Cpp are these:
\lstinline| .cpp, .C, .cxx, .ii, .ixx, .ipp, .i++, .inl, .h, .hh|

Currently this set of suffixes is compiled in the plugin (in file
\texttt{frama\_Clang\_register.ml}) and can only be changed by
recompiling and reinstalling the plugin.

\section{Frama-clang executable}
The plug-in operates by invoking the executable \irg (which must be on the system \verb|$PATH|)
on each file identified as \Cpp, in turn. 
For each file it produces a temporary output file containing an equivalent \C AST, which is then translated and passed on as input to \FramaC. 
This executable is a single-file-at-a-time command-line executable only. 
Various options control its behavior.

The file-system path identifying the executable is provided by the \textbf{-cxx-clang-command <cmd>}
option and is \irg by default. The path may be absolute; if it is a relative path, it is found by searching the system \verb|$PATH|.


The PARSING section of the output of \lstinline|frama-c -kernel-h|
lists some options for controlling the behavior described above. This is notably
the case for:
\begin{itemize}
\item \lstinline|-cpp-extra-args| which contains arguments to be passed to the
preprocessor (e.g. \lstinline|-D| macro definitions or \lstinline|-I|
search path directives). In case the project under analysis mixes C and
C++ files which
require distinct preprocessor directives, it is possible to use the
\framaclang-specific option \lstinline|-fclang-cpp-extra-args|.
In that case, \framaclang will not consider \lstinline|-cpp-extra-args| at all.
See section~\ref{sec:include-directories} for more information.
\item \lstinline|-machdep| which indicates the architecture on
which the program is supposed to run. See section~\ref{sec:bit} for more
information
\end{itemize}

Apart from \lstinline|-fclang-cpp-extra-args|, and
\lstinline|-cxx-clang-command|, \framaclang options governing the
parsing of C++ files are:
\begin{itemize}
\item \lstinline|-cxx-c++stdlib-path|, the path where \framaclang standard C++
  headers are located.
\item \lstinline|-cxx-cstdlib-path|, the path where \FramaC standard C headers
  are located
\item \lstinline|-cxx-nostdinc|, instructs \irg not to consider \framaclang and
\FramaC headers (i.e. fall back to system headers).
\end{itemize}

\section{Frama-clang options}

The options controlling \framaclang are of four sorts:
\begin{itemize}
\item options known to the \FramaC kernel
\item options the  \framaclang plug-in has registered with the \FramaC kernel.
These also are recognized by the FramaCcommand-line.
\item options known to \irg directly (and not to FramaC, These must be 
included in the internal command that invokes \irg using the \lstinline|-cpp-extra-args| option. These options are described in \S\ref{sec:standalone}.
\item \clang options, which must also be supplied using the \lstinline|-cpp-extra-args| option, and are passed through \irg to \clang. See \S\ref{sec:standalone}.
\end{itemize}

The options in the first two categories are processed by the \FramaC kernel
when listed on the \FramaC command-line. 
The use of the FramaCcommand-line is described in the core \FramaC
user guide.
There are many kernel options that affect all plugins and many options specific to \framaclang.
The command \\
\centerline{\lstinline|frama-c -kernel-h|} \\
shows all kernel options; the command\\
\centerline{\lstinline|frama-c -fclang-h|} \\
shows all \framaclang specific options.

The most important of the options are these:
\begin{itemize}
\item \lstinline|--help|  or \lstinline|-h| -- introduction to \FramaC help
    \item \lstinline|-kernel-h|, \lstinline|-fclang-h| -- help information about FramaC the FramaCkernel and the \framaclang plug-in
    \item \lstinline|-print| -- prints out the input file seen by FramaC when
 \framaclang is being used this is the input file after pre-processing and
 translation from \Cpp to \C. Thus this output can be useful to see (and debug)
 the results of \framaclang's transformations.
    \item \lstinline|-kernel-warn-key=annot-error=<val>| sets the behavior of \FramaC, including \framaclang, when a parsing error is encountered. The default value (set by the kernel) is \texttt{abort}, which terminates processing upon the first error; a more useful alternative is \texttt{active}, which reports errors but continues processing further annotations.
	\item \lstinline|-machdep <arg>| -- sets the target machine architecture, cf. \S\ref{sec:bit}
	\item \lstinline|-kernel-msg-key <categories>| -- sets the amount of informational messages according to different categories of messages.
See \lstinline|-kernel-msg-key help| for a list of informational categories.
	\item \lstinline|-kernel-warn-key <categories>| -- sets the amount and behavior of warnings.\\ See \lstinline|-kernel-warn-key help| for a list of warning categories.
	\item \lstinline|-fclang-msg-key <categories>| -- sets the amount of informational messages according to different categories of messages.
See \lstinline|-fclang-msg-key help| for a list of informational categories.
	\item \lstinline|-fclang-warn-key <categories>| -- sets the amount and behavior of warnings.\\ See \lstinline|-fclang-warn-key help| for a list of warning categories.
	\item \lstinline|-fclang-verbose <n>| -- sets the amount of information
          from the \framaclang plug-in
	\item \lstinline|-fclang-debug <n>| -- sets the amount of debug
          information from the \framaclang plug-in
	\item \lstinline|-annot| -- enables processing \acslpp annotations (enabled by default)
	\item \lstinline|-no-annot| -- disables processing \acslpp annotations
        \item \lstinline|-cxx-unmangling <key>| indicates how mangled C++ symbols will be displayed
          by \FramaC pretty-printing. \texttt{key} can be one of:
          \begin{itemize}
            \item \texttt{help}: outputs the list of existing \texttt{key} with a short description
            \item \texttt{fully-qualified}: each symbol is displayed with its fully-qualified
              C++ name
            \item \texttt{without-qualifier}: each symbol is displayed with its unqualified name.
              This gives shorter, but more ambiguous outputs.
            \item \texttt{none}: no demangling is performed, symbols are displayed as seen in
              the AST
          \end{itemize}
        \item \lstinline|-cxx-parseable-output| indicates that the pretty-printed code resulting
          from the translation should be able to be parsed again by \FramaC. This implies
          \lstinline|-cxx-unmangling none|.
\end{itemize}

Note that the \FramaC option \verb|-no-pp-annot| is ignored by \framaclang. Preprocessing is always performed on the source input (unless annotations are ignored entirely using \verb|-no-annot|).
\section{Include directories}\label{sec:include-directories}

By default \irg is given the paths to the two directories containing the
\framaclang and \FramaC header files, which include \acslpp specifications for the \Cpp library functions. The default paths (namely \verb|$FRAMAC_SHARE/libc++| and
\verb|$FRAMAC_SHARE/libc|) to these directories
can be overriden by the \framaclang options \lstinline|-cxx-c++stdlib-path| and
 \lstinline|-cxx-cstdlib-path| options.

Users typically have additional header files for their own projects.
These are supplied to the \framaclang preprocessor using the option \lstinline|-cpp-extra-args|.

You can use \lstinline|-fclang-cpp-extra-args| instead of \lstinline|cpp-extra-args|; multiple such options also have a cumulative effect. 
The \framaclang option only affects the \framaclang plugin, whereas 
\lstinline|-cpp-extra-args| may be seen by other plugins as well, if such plugins do their own preprocessing. Also note that the presence of any instance of \lstinline|-fclang-cpp-extra-args| will cause uses of \lstinline|-cpp-extra-args| to be ignored. 

The system header files supplied by \framaclang does not include all \Cpp system files. Omissions should be reported to the \FramaC team.

As an example, to perform \lstinline|wp| checking of files \lstinline|a.cpp| and \lstinline|inc/a.h|, one might use the command-line \\
\centerline{\texttt{frama-c -cpp-extra-args="-Iinc" -wp a.cpp}}

\section{32 and 64-bit targets}
\label{sec:bit}

\acslpp is for the most part machine-independent. 
There are some features of \Cpp that can be environment-dependent, such as the sizes of fundamental datatypes. 
Consequently, \FramaC has some options that allow the user to state what machine target is intended. 

\begin{itemize}
\item The \lstinline|-machdep| option to \FramaC. See the allowed values using the command\\
\centerline{ \lstinline|frama-c -machdep help|.}
 For example, with a value of \lstinline|x86_32|, \lstinline|sizeof(long)| has a value of 4, whereas with the option \lstinline|-machdep x86_64|, \lstinline|sizeof(long)| has a value of 8.
%%\item Alternately, the value of \lstinline|-machdep| can be set instead using an environment variable: \lstinline|__FC_MACHDEP|. The variable can be set either in the shell environment or on the command line with \lstinline|-D__FC_MACHDEP=...|

\end{itemize}


\section{Warnings, errors, and informational output}

Output messages arise from multiple places: from the \framaclang plugin,
from the \irg lexer and parser, from the \clang parser, and from the
 \FramaC kernel (as well as from any other plugins that may be invoked, such as
the \texttt{wp} plug-in). 
They are controlled by a number of options within the \FramaC kernel and each plugin.
Remember that \clang and \irg options must be put in the \lstinline|-cpp-extra-args| option.

Output messages, including errors, are written to standard out, not to standard error. 

\subsection{Errors}

Error messages are always output. 
The key question is whether processing stops or continues upon encountering an error. 
Continuing can result in a cascade of unhelpful error messages, but stopping immediately can hide errors that occur later in source files.
\begin{itemize}
\item \lstinline|--stop-annot-error| is a \irg option that causes prompt termination on annotations errors (the \irg default is to continue); this does not respond to errors in \Cpp code
\item \lstinline|-kernel-warn-key=annot-error=abort| is a \framaclang plug-in option that will invoke \irg with \lstinline|--stop-annot-error|.  \lstinline|error| and \lstinline|error_once| (instead of \lstinline|abort|) have the same effect; other values for the key will allow continuing after errors. The default is \texttt{abort}.
\end{itemize}

\subsection{Warnings}

%The various categories of warnings from \framaclang can be seen with the
%command \\ \centerline{\lstinline|frama-c -fclang-warn-key help|}
%Warning messages are emitted by default.

Warning messages from \irg can be controlled with the \lstinline|-warn| option of \irg.
% Note in the current version
%, or, equivalently, the \lstinline|-fclang-warn-key=parse| option of FramaC

\begin{itemize}
\item \lstinline|-Werror| is a clang and \irg option that causes any parser warnings to be treated as errors
\item \lstinline|-w| is a clang and \irg option that causes any parser warnings to be ignored
\end{itemize}

\textit{The \clang options are not currently integrated with the \FramaC
  warning and error key system.}

\subsection{Informational output}

\textit{This section is not yet written}

\textit{The \clang informational output is not currently integrated with the
 \FramaC warning and error key system.}

\chapter{Running the \framaclang front-end standalone}
\label{sec:standalone}

In normal use within \FramaC, the \irg executable is
invoked automatically. However, it can also be run standalone.
In this mode it accepts command-line options and a single input file;
it produces a C AST representing the translated \Cpp, in a text format similar to Cabs.

The exit code from \irg is
\begin{itemize}
\item 0 if processing is successful, including if only warnings or informational messages are emitted
\item 0 if there are some non-fatal errors but \lstinline|--no-exit-code| is enabled (the default)
\item 1 if there are some non-fatal errors but \lstinline|--exit-code| is enabled, or if there are warnings and \lstinline|-Werror| is enabled, but \lstinline|-w| is not.
\item 2 if there are fatal errors
\end{itemize}
Fatal errors are those resulting from misconfiguration of the system; non-fatal errors are the result of errors in the user input (e.g. parsing errors).

The \lstinline|-Werror| option causes warnings to be treated as errors. 

All output is sent to the standard output.\footnote{Currently clang output goes to std err.}

\section{\irg specific options}
\label{sec:fcloptions}

These options are specific to \irg.
\begin{itemize}
	\item \lstinline|-h| -- print help information
	\item \lstinline|-help| -- print more help information
	\item \lstinline|-{-}version| -- print version information
	\item \lstinline|-o <file>| -- specifies the name and location of the output file (that is, the file to contain the generated AST). The output path may be absolute or relative to the current working directory. \textit{This option is required.}
        \item \lstinline|-v| -- verbose output
	\item \lstinline|-{-}stop-annot-error| -- if set, then parsing stops on the first error; default is off
	
\end{itemize}

\section{Clang options}

Frama-Clang is built on the \clang \Cpp parser. 
As such, the \irg executable accepts the clang
compiler options and passes them on to clang. There are many of these.
Many of these are irrelevant to \framaclang as they relate to 
code generation, whereas \framaclang only uses \clang for parsing, name
and type resolution, and producing the AST.
You can see a list of options by running 
\lstinline|framaCIRGen -help|

The most significant \clang options are these:
\begin{itemize}
	\item \lstinline|-I <dir>| -- adds a directory to the include file search path. Using absolute paths is recommended; relative paths are relative to the current working directory.
	\item \lstinline|-w| -- suppress clang warnings
	\item \lstinline|-Werror| -- treat warnings as errors
\end{itemize}

Although \clang can process languages other than \Cpp, \Cpp is the only one usable with \framaclang.

\section{Default command-line}

The launching of \irg by \FramaC includes the following options by default. The FramaCoption \lstinline|-fclang-msg-key=clang| will show (among other information) the internal command-line being invoked.
\begin{itemize}
\item \verb|-target <target>| with the target being set according to
  the \lstinline|-machdep| and \lstinline|-target| options given to
  \FramaC (cf. \S\ref{sec:bit})
\item \verb|-D__FC_MACHDEP_86_32| -- also set according to the chosen
  architecture. The corresponding \verb|__FC_MACHDEP_*| macro is used in
  \FramaC- and \framaclang provided standard headers for architecture-specific
  features.
\item \verb|-std=c++11| -- target C++11 features
\item \verb|-nostdinc| -- use \framaclang and \FramaC system header files, and not the compiler's own header files
\item \verb|-I$FRAMAC_SHARE/frama-clangs/libc++ -I$FRAMAC_SHARE/libc| -- include the \framaclang and \FramaC header files, which contain system library definitions with \acslpp annotations (the paths used are controlled by the FramaCoptions \lstinline|-cxx-c++stdlib-path| and \lstinline|-cxx-cstdlib-path|).
\item \verb|--annot| or \verb|--no-annot| according to the \verb|-annot| or \verb|-no-annot| \FramaC kernel option
\item \verb|-stop-annot-error| if the corresponding option (\lstinline|-fclang-warn-key=annot-error=abort|) is given to \FramaC
\item options to set the level of info messages and warning messages,
 based on options on the \FramaC command-line
\end{itemize}
%\section{Custom ASTs}
%\lstset{keepspaces=true}
%In standard mode, \FramaC invokes \irg on a file, producing an AST in intermediate format, and the reads that intermediate file into \FramaC to complete the processing.
%If some manipulation of the AST intermediate is needed, those two steps can be performed separately as follows:
%\begin{itemize}
%\item Produce an intermediate AST \lstinline|f.ast| for a given input file \lstinline|f.cpp| using the command \\
%\centerline{\lstinline|framaCIRGen <options> -o f.ast f.cpp|}
%\item Manipulate \lstinline|f.ast| as desired.
%\item Run \FramaC on the AST using the command \\
%\centerline{\lstinline|frama-c <options> -cpp-command "cat f.ast" f.cpp|}
%\end{itemize}
%
%If you have multiple files, do the following:
%\begin{itemize}
%\item Create the ast files for a group of files in \$files:\\
%\centerline{\bf \texttt{ for f in \$files; do framaCIRGen <options> -o \${f\%.*}.ast \$f ; done }}
%\item Manipulate the resulting .ast files as needed
%\item Execute a command like \\
%\centerline{\bf \texttt{frama-c -cpp-command "`pwd`/ct" \$files}}
%\end{itemize}
%where \lstinline|ct| is an executable similar to\\
%\centerline{\bf \texttt{for f in \$@ ; do cat \$\{f\%.*\}.ast ; done}}

