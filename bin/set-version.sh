#!/bin/bash
##########################################################################
#                                                                        #
#  This file is part of Frama-Clang                                      #
#                                                                        #
#  Copyright (C) 2012-2025                                               #
#    CEA (Commissariat à l'énergie atomique et aux énergies              #
#         alternatives)                                                  #
#                                                                        #
#  you can redistribute it and/or modify it under the terms of the GNU   #
#  Lesser General Public License as published by the Free Software       #
#  Foundation, version 2.1.                                              #
#                                                                        #
#  It is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU Lesser General Public License for more details.                   #
#                                                                        #
#  See the GNU Lesser General Public License version 2.1                 #
#  for more details (enclosed in the file LICENSE).                      #
#                                                                        #
##########################################################################

FCLANG_NEXT=$1
FC_NEXT=$2
FC_NEXT_NAME=$3

if ! test -f frama-clang.opam.template; then
    echo "This script must be run from Frama-Clang root directory"
    exit 2;
fi

if test -z "$FCLANG_NEXT" -o -z "$FC_NEXT" -o -z "$FC_NEXT_NAME"; then
    echo "Missing argument. Usage is:"
    echo "./bin/set-version.sh fclang-version fc-version fc-version-name"
    exit 2;
fi

if command -v gsed &>/dev/null; then
  SED=gsed
else
  if sed --version 2>/dev/null | grep -q GNU; then
    SED=sed
  else
    echo "GNU sed required"
    exit 1
  fi
fi

FC_NEXT_ROOT=$($SED -e "s/^\\([0-9]*\.[0-9]*\\).*/\\1/" <<< $FC_NEXT)
FC_NEXT_MAJOR=$($SED -e "s/^\\([0-9]*\\)\..*/\\1/" <<< $FC_NEXT)

if test -z "$FC_NEXT_ROOT" -o -z "FC_NEXT_MAJOR"; then
    echo "Unrecognized fc-version number (expecting X.Y)"
    exit 2;
fi

FC_NEXT_NEXT_MAJOR=$(($FC_NEXT_MAJOR + 1))

$SED -i -e "s/^ *version: .*/version: \"$FCLANG_NEXT\"/" frama-clang.opam.template
$SED -i -e "s/^ *version: .*/version: \"$FCLANG_NEXT\"/" frama-clang.opam
$SED -i -e "s/(\"frama-c\" .*/(\"frama-c\" (and (>= $FC_NEXT_ROOT~) (< $FC_NEXT_NEXT_MAJOR.0~)))/" dune-project
$SED -i -e "s/- Frama-C version.*/- Frama-C version $FC_NEXT $FC_NEXT_NAME/" README.md
echo $FCLANG_NEXT > doc/userman/FCLANG_VERSION
echo $FC_NEXT > doc/userman/FC_VERSION
echo $FC_NEXT_NAME > doc/userman/FC_VERSION_NAME
