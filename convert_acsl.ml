(**************************************************************************)
(*                                                                        *)
(*  This file is part of Frama-Clang                                      *)
(*                                                                        *)
(*  Copyright (C) 2012-2025                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file LICENSE).                      *)
(*                                                                        *)
(**************************************************************************)

open Intermediate_format
open Logic_ptree

let find_loc_glob_annot = function
  | Dfun_or_pred(loc,_)
  | Daxiomatic(loc,_,_)
  | Dtype (loc,_)
  | Dlemma(loc,_,_,_,_,_)
  | Dinvariant(loc,_)
  | Dtype_annot(loc,_)
  | Dmodel_annot(loc,_)
  | Dvolatile(loc,_,_,_) -> loc

let convert_var env is_extern_c vi =
  match vi.logic_var_lv_kind with
    | LVGlobal | LVCGlobal ->
      if is_extern_c then vi.logic_var_lv_name.decl_name
      else
        let n, tk
          = Convert_env.typedef_normalize env vi.logic_var_lv_name TStandard
        in Mangling.mangle n tk None
    | LVFormal | LVQuant | LVLocal | LVCLocal | LVBuiltin ->
      vi.logic_var_lv_name.decl_name

let convert_logic_label = function
  | StmtLabel s -> s
  | LogicLabel s -> s

let convert_logic_label_opt = function
  | Some lab -> Some (convert_logic_label lab)
  | None -> None

let convert_ikind env = function
  | IBool -> env, LTint Cil_types.IBool
  | IChar_u | IChar_s | IChar -> env, LTint Cil_types.IChar
  | IUChar -> env, LTint Cil_types.IUChar
  | ISChar -> env, LTint Cil_types.ISChar
  | IChar16 -> env, LTint (Cil.intKindForSize 2 true)
  | IChar32 -> env, LTint (Cil.intKindForSize 4 true)
  | IWChar_u | IWChar_s | IWChar ->
    let env = Convert_env.memo_wchar env in
    env, LTnamed ("wchar_t", [])
  | IShort -> env, LTint Cil_types.IShort
  | IUShort -> env, LTint Cil_types.IUShort
  | IInt -> env, LTint Cil_types.IInt
  | IUInt -> env, LTint Cil_types.IUInt
  | ILong -> env, LTint Cil_types.ILong
  | IULong -> env, LTint Cil_types.IULong
  | ILongLong -> env, LTint Cil_types.ILongLong
  | IULongLong -> env, LTint Cil_types.IULongLong

let convert_fkind = function
  | FFloat -> Cil_types.FFloat
  | FDouble -> Cil_types.FDouble
  | FLongDouble -> Cil_types.FLongDouble

let int_to_char c =
  let open Int64 in
  let char_max = of_int 256 in
  let mk_char c =
    Char.unsafe_chr (Option.get (unsigned_to_int (unsigned_rem c char_max)))
  in
  let rec aux seq c =
    let seq = Seq.cons (mk_char c) seq in
    let c = unsigned_div c char_max in
    if equal c zero then String.of_seq seq else aux seq c
  in
  (* NB: intermediate_format uses int there, but, at least for wide chars, it's
     better to stick with int64, that allows for unsigned operations. The whole
     thing would probably need a complete overhaul, though.
  *)
  aux Seq.empty (Int64.of_int c)

let convert_char c = "'" ^ int_to_char c ^ "'"

let convert_wchar c = "L'" ^ int_to_char c ^ "'"

let lexpr env node = { lexpr_loc = Convert_env.get_loc env; lexpr_node = node }

let convert_logic_constant = function
  | LCInt v -> IntConstant v
  | LStr s -> StringConstant s
  | LWStr _s ->
    Frama_Clang_option.not_yet_implemented "Wide string support in logic"
  | LChr c -> IntConstant (convert_char c)
  | LWChr c -> IntConstant (convert_wchar c)
  | LCReal s -> FloatConstant s
  | LCEnum (v,_) -> IntConstant (Int64.to_string v)
    (* TODO: add support into ACSL for that? *)

let convert_cst_array_size env c =
  lexpr env (PLconstant (convert_logic_constant c))

let rec convert_logic_type env = function
  | Lvoid -> env, LTvoid
  | Lint ik -> convert_ikind env ik
  | Lfloat fk -> env, LTfloat (convert_fkind fk)
  | Larray (t,dim) ->
    let env, t = convert_logic_type env t in
    let dim = Option.map (convert_cst_array_size env) dim in
    env, LTarray(t,dim)
  | Lpointer t | Lreference t ->
    let env, t = convert_logic_type env t in
    env, LTpointer t
  | Lenum e ->
    let e, tk = Convert_env.typedef_normalize env e TStandard
    in env, LTenum (Mangling.mangle e tk None)
  | Lstruct (s,t) ->
    let s,t = Convert_env.typedef_normalize env s t in
    let cname =
      if Convert_env.is_extern_c_aggregate env s t then s.decl_name
      else Mangling.mangle s t None
    in
    env, LTstruct cname
  | Lunion (u,t) ->
    let u, t = Convert_env.typedef_normalize env u t in
    let cname =
      if Convert_env.is_extern_c_aggregate env u t then u.decl_name
      else Mangling.mangle u t None
    in
    env, LTunion cname
  | LCnamed (n,extern_c) ->
      let name = if extern_c then n.decl_name
      else (* change only the template parameters in the qualification *)
        let n, tk = Convert_env.typedef_normalize env n TStandard
        in Mangling.mangle n tk None
      in
      env, LTnamed (name,[])
  | Lnamed(t,extern_c,args) ->
    let name =
      if extern_c then t.decl_name
      else
        let t, tk = Convert_env.typedef_normalize env t TStandard
        in Mangling.mangle t tk None
    in
    let env, args = Convert_env.env_map convert_logic_type env args in
    env, LTnamed(name,args)
  | Lvariable s -> env, LTnamed(s,[])
  | Lboolean -> env, LTboolean
  | Linteger -> env, LTinteger
  | Lreal -> env, LTreal
  | Larrow(args,rt) ->
    let env, args = Convert_env.env_map convert_logic_type env args in
    let env, rt = convert_logic_type env rt in
    env, LTarrow(args, rt)

let convert_logic_param env p =
  let env, typ = convert_logic_type env p.la_type in
  env, (typ, p.la_name)

type bkind = Relation | Logic | Arithmetic

let bo_kind = function
  | BOPlus | BOMinus | BOTimes | BODivide | BOModulo | BOBitOr | BOBitAnd
  | BOBitExclusiveOr | BOLeftShift | BORightShift
      -> Arithmetic
  | BOLess | BOLessOrEqual | BOEqual | BODifferent
  | BOGreaterOrEqual | BOGreater
      -> Relation
  | BOLogicalAnd | BOLogicalOr -> Logic
  | BOComma -> Frama_Clang_option.fatal "Comma operator in ACSL++ expression"

let convert_bop_arith = function
  | BOPlus -> Badd
  | BOMinus -> Bsub
  | BOTimes -> Bmul
  | BODivide -> Bdiv
  | BOModulo -> Bmod
  | BOBitOr -> Bbw_or
  | BOBitAnd -> Bbw_and
  | BOBitExclusiveOr -> Bbw_xor
  | BOLeftShift -> Blshift
  | BORightShift -> Brshift
  | _ -> Frama_Clang_option.fatal "not a binary arithmetic operation"

let convert_bop_relation = function
  | BOLess -> Lt
  | BOLessOrEqual -> Le
  | BOEqual -> Eq
  | BODifferent -> Neq
  | BOGreater -> Gt
  | BOGreaterOrEqual -> Ge
  | _ -> Frama_Clang_option.fatal "not a relation"

let convert_rel = function
  | Rlt -> Lt
  | Rgt -> Gt
  | Rle -> Le
  | Rge -> Ge
  | Req -> Eq
  | Rneq -> Neq

let convert_var_decl env v =
  let env, typ = convert_logic_type env v.lv_type in
  let name = v.logic_var_def_lv_name.decl_name in
  env, (typ, name)

let convert_reference env typ e =
  match typ with
    | LVReference _ | RVReference _ -> PLunop(Ustar, lexpr env e)
    | _ -> e

let convert_logic_reference env typ e =
  match typ with
    | Lreference _ -> PLunop(Ustar, lexpr env e)
    | _ -> e

(* ACSL parser normalizes conjunctions and disjunctions. We do the same here
   in order to ensure syntactical equality of ACSL predicates
   when parsed either directly or through framaCIRGen in an extern "C". *)
let rec pland ce1 ce2 =
  match ce2.lexpr_node with
  | PLand(ce3,ce4) ->
    let lexpr_loc = fst ce1.lexpr_loc, snd ce3.lexpr_loc in
    let lexpr_node = pland ce1 ce3 in
    PLand ({ lexpr_loc; lexpr_node}, ce4)
  | _ -> PLand (ce1, ce2)

let rec plor ce1 ce2 =
  match ce2.lexpr_node with
  | PLor(ce3, ce4) ->
    let lexpr_loc = fst ce1.lexpr_loc, snd ce3.lexpr_loc in
    let lexpr_node = plor ce1 ce3 in
    PLor ({ lexpr_loc; lexpr_node }, ce4)
  | _ -> PLor (ce1, ce2)

let rec convert_logic_expr env e =
  let open Current_loc.Operators in
  let env = Convert_env.set_loc env e.loc in
  let lexpr_loc = Convert_env.get_loc env in
  let<> UpdatedCurrentLoc = lexpr_loc in
  let env, lexpr_node = convert_logic_expr_node env e.node in
  let anonymous = { lexpr_loc; lexpr_node } in
  let res =
  List.fold_right
    (fun s res -> { lexpr_loc; lexpr_node = PLnamed(s,res) })
    e.names anonymous
  in
  env, res
and convert_logic_expr_node env = function
  | TConst c -> env, PLconstant(convert_logic_constant c)
  | TLval lv -> convert_logic_lval env lv
  | TSizeOf t ->
    let env, typ = convert_logic_type env t in
    env, PLsizeof typ
  | TSizeOfStr s ->
    let cs = lexpr env (PLconstant(StringConstant s)) in
    env, PLsizeofE(cs)
  | TUnOp((UOPostInc|UOPostDec|UOPreInc|UOPreDec),_) ->
    Convert_env.fatal env "Side effect operation in ACSL++ expression"
  | TUnOp((UOCastNoEffect _ | UOCastDeref | UOCastToVoid | UOCastInteger _
          | UOCastDerefInit | UOCastEnum _ | UOCastFloat _ | UOCastC _),_) ->
    Convert_env.fatal env
      "Logic casts are not supposed to use TUnOp but TCastE"
  | TUnOp(UOOpposite,e) ->
    let env, e = convert_logic_expr env e in
    env, PLunop(Uminus,e)
  | TUnOp(UOBitNegate,e) ->
    let env, e = convert_logic_expr env e in
    env, PLunop(Ubw_not,e)
  | TUnOp(UOLogicalNegate,e) ->
    let env, e = convert_logic_expr env e in
    env, PLnot e
  | TBinOp(bo,e1,e2) when bo_kind bo = Arithmetic ->
    let cbo = convert_bop_arith bo in
    let env, ce1 = convert_logic_expr env e1 in
    let env, ce2 = convert_logic_expr env e2 in
    env, PLbinop(ce1,cbo,ce2)
  | TBinOp(bo,e1,e2) when bo_kind bo = Relation ->
    let cbo = convert_bop_relation bo in
    let env, ce1 = convert_logic_expr env e1 in
    let env, ce2 = convert_logic_expr env e2 in
    env, PLrel(ce1,cbo,ce2)
  | TBinOp(BOLogicalAnd,e1,e2) ->
    let env, ce1 = convert_logic_expr env e1 in
    let env, ce2 = convert_logic_expr env e2 in
    env, pland ce1 ce2
  | TBinOp(BOLogicalOr,e1,e2) ->
    let env, ce1 = convert_logic_expr env e1 in
    let env, ce2 = convert_logic_expr env e2 in
    env, plor ce1 ce2
  | TBinOp _ -> Convert_env.fatal env "Unknown binary operator in logic"
  | TCastE(typ,e) ->
    let env, ctyp = convert_logic_type env typ in
    let env, ce = convert_logic_expr env e in
    env, PLcast(ctyp,ce)
  | TAddrOf lv | TStartOf lv ->
    let env, e = convert_logic_lval env lv in
    (* simplify memory accesses introduced by handling of references *)
    (match e with
      | PLunop(Ustar,t) -> env, t.lexpr_node
      | _ -> env, PLunop(Uamp,lexpr env e))
  | TFieldAccess (e,(TField(f,TNoOffset)|TModel(f,TNoOffset))) ->
    let env, ce = convert_logic_expr env e in env, PLdot(ce,f)
  | TFieldAccess _ ->
    Convert_env.fatal env "Unexpected offset in field access"
  | TFalse -> env, PLfalse
  | TTrue -> env, PLtrue
  | TApp(f,l,args,extern_c) ->
    (* TODO: if f is an extern C (or built-in), do not mangle it *)
    let fname =
      if extern_c then f.decl_name
      else
        let f, t = Convert_env.typedef_normalize env f TStandard
        in Mangling.mangle f t None
    in
    let labels = List.map (fun l -> convert_logic_label l.snd) l in
    let env, args = Convert_env.env_map convert_logic_expr env args in
    env, PLapp(fname,labels,args)
  | TLambda (vars,t) ->
    let env, quants = Convert_env.env_map convert_var_decl env vars in
    let env, e = convert_logic_expr env t in
    env, PLlambda(quants,e)
  | TDataCons(name,args) ->
    let name, t = Convert_env.typedef_normalize env name TStandard in
    let name = Mangling.mangle name t None in
    let env, args = Convert_env.env_map convert_logic_expr env args in
    env, PLapp(name,[],args)
  | TIf(cond,etrue,efalse) ->
    let env, ccond = convert_logic_expr env cond in
    let env, cetrue = convert_logic_expr env etrue in
    let env, cefalse = convert_logic_expr env efalse in
    env, PLif(ccond,cetrue,cefalse)
  | TAt(t,l) ->
    let env, t = convert_logic_expr env t in
    let l = convert_logic_label l in
    env, PLat(t,l)
  | TBase_addr(l,t) ->
    let l = convert_logic_label_opt l in
    let env, t = convert_logic_expr env t in
    env, PLbase_addr(l,t)
  | TOffset(l,t) ->
    let l = convert_logic_label_opt l in
    let env, t = convert_logic_expr env t in
    env, PLoffset(l,t)
  | TBlock_length(l,t) ->
    let l = convert_logic_label_opt l in
    let env, t = convert_logic_expr env t in
    env, PLblock_length(l,t)
  | TNull -> env, PLnull
  | TLogic_coerce(_,t) ->
    (* should be transparent at the parsed logic tree level. *)
    let env, e = convert_logic_expr env t in
    env, e.lexpr_node
  | TUpdate(obj,off,t) ->
    let env, obj = convert_logic_expr env obj in
    let env, path = path_of_offset env off in
    let env, value = convert_logic_expr env t in
    env, PLupdate(obj,path,PLupdateTerm value)
  | TEmpty_set -> env, PLempty
  | TSet l ->
      let env, l = Convert_env.env_map convert_logic_expr env l in
      env, PLset l
  | TUnion l ->
    let env, l = Convert_env.env_map convert_logic_expr env l in
    env, PLunion l
  | TInter l ->
    let env, l = Convert_env.env_map convert_logic_expr env l in
    env, PLinter l
  | TComprehension (t,quants,pred) ->
    let env, quants = Convert_env.env_map convert_var_decl env quants in
    let env, t = convert_logic_expr env t in
    let env, pred = Convert_env.env_opt convert_pred_named env pred in
    env, PLcomprehension (t,quants,pred)
  | TRange(l,h) ->
    let env, l = Convert_env.env_opt convert_logic_expr env l in
    let env, h = Convert_env.env_opt convert_logic_expr env h in
    env, PLrange(l,h)
  | TLet(info,t) ->
    let env, body = convert_inner_body env info in
    let name = info.li_name.decl_name in
    let env, rest = convert_logic_expr env t in
    env, PLlet(name,body,rest)
  | TCoerce _ | TCoerceE _ ->
    Frama_Clang_option.not_yet_implemented "coercions"

and convert_logic_lval env lv =
  let env, base = convert_logic_lhost env lv.base_support in
  convert_logic_offset env base lv.offset

and convert_logic_lhost env = function
  | TVar v ->
    (match v.logic_var_lv_kind with
      | LVCGlobal ->
        let is_extern_c, typ =
          Convert_env.get_global_var env v.logic_var_lv_name
        in
        let var = PLvar (convert_var env is_extern_c v) in
        env, convert_reference env typ var
      | LVCLocal ->
        let var = PLvar v.logic_var_lv_name.decl_name in
        let typ = Convert_env.get_local_var env v.logic_var_lv_name.decl_name in
        env, convert_reference env typ var
      | _ ->
        env, PLvar v.logic_var_lv_name.decl_name
    )
  | TCFun(n,s) ->
    let is_extern_c, _typ = Convert_env.get_func env n in
    let name =
      if is_extern_c
      then n.decl_name
      else
        let n, t = Convert_env.typedef_normalize env n TStandard in
        Mangling.mangle n t (Some (FKFunction,s))
    in
    env, PLvar name
  | TResult typ -> env, convert_logic_reference env typ PLresult
  | TMem t -> let env, t = convert_logic_expr env t in env, PLunop(Ustar,t)

and convert_logic_offset env base = function
  | TNoOffset -> env, base
  | TField(s,o) | TModel(s,o) ->
    let base = PLdot(lexpr env base,s) in
    convert_logic_offset env base o
  | TBase(b,t,o) ->
    let b, t = Convert_env.typedef_normalize env b t in
    let base = PLdot(lexpr env base,("_frama_c_" ^ Mangling.mangle b t None)) in
    convert_logic_offset env base o
  | TVirtualBase(b,t,o) ->
    let b, t = Convert_env.typedef_normalize env b t in
    let base = PLdot(lexpr env base,("_frama_c_" ^ Mangling.mangle b t None)) in
    convert_logic_offset env base o
  | TDerived(_ (* d *),_ (* td *),_ (* b *),_ (* tb *),_ (* o *)) ->
    Convert_env.fatal env
      "casts to derived classes are not supported for the moment"
  | TIndex(t,o) ->
    let env, i = convert_logic_expr env t in
    let base = PLarrget(lexpr env base,i) in
    convert_logic_offset env base o

and path_of_offset env = function
  | TNoOffset -> env, []
  | TField(s,o) | TModel(s,o) ->
    let env, path = path_of_offset env o in
    env, PLpathField s :: path
  | TBase(b,t,o) ->
    let env, path = path_of_offset env o in
    let b, t = Convert_env.typedef_normalize env b t in
    env, PLpathField ("_frama_c_" ^ Mangling.mangle b t None) :: path
  | TVirtualBase(b,t,o) ->
    let env, path = path_of_offset env o in
    let b, t = Convert_env.typedef_normalize env b t in
    env, PLpathField ("_frama_c_" ^ Mangling.mangle b t None) :: path
  | TDerived(_ (* d *),_ (* td *),_ (* b *),_ (* tb *),_ (* o *)) ->
    Convert_env.fatal env
      "casts to derived classes are not supported for the moment"
  | TIndex(t,o) ->
    let env, i = convert_logic_expr env t in
    let env, path = path_of_offset env o in
    env, PLpathIndex(i) :: path

and convert_pred_named env p =
  let open Current_loc.Operators in
  let env = Convert_env.set_loc env p.pred_loc in
  let pred_loc = Convert_env.get_loc env in
  let<> UpdatedCurrentLoc = pred_loc in
  let env, cp = convert_pred env p.pred_content in
  let cp =
    List.fold_right
      (fun s acc -> PLnamed(s,{ lexpr_node = acc; lexpr_loc = pred_loc }))
      p.pred_name cp
  in
  env, { lexpr_node = cp; lexpr_loc = pred_loc }

and convert_pred env = function
  | Pfalse -> env, PLfalse
  | Ptrue -> env, PLtrue
  | PApp(p,labs,args,is_extern_c) ->
    let pname = if is_extern_c then p.decl_name
    else
      let p, t = Convert_env.typedef_normalize env p TStandard in
      Mangling.mangle p t None
    in let clabs = List.map (fun l -> convert_logic_label l.snd) labs in
    let env, cargs = Convert_env.env_map convert_logic_expr env args in
    env, PLapp(pname,clabs,cargs)
  | Pseparated l ->
    let env, l = Convert_env.env_map convert_logic_expr env l in
    env, PLseparated l
  | Prel(rel,t1,t2) ->
    let rel = convert_rel rel in
    let env, t1 = convert_logic_expr env t1 in
    let env, t2 = convert_logic_expr env t2 in
    env, PLrel(t1,rel,t2)
  | Pand(p1,p2) ->
    let env, p1 = convert_pred_named env p1 in
    let env, p2 = convert_pred_named env p2 in
    env, pland p1 p2
  | Por(p1,p2) ->
    let env, p1 = convert_pred_named env p1 in
    let env, p2 = convert_pred_named env p2 in
    env, plor p1 p2
  | Pxor(p1,p2) ->
    let env, p1 = convert_pred_named env p1 in
    let env, p2 = convert_pred_named env p2 in
    env, PLxor(p1,p2)
  | Pimplies(p1,p2) ->
    let env, p1 = convert_pred_named env p1 in
    let env, p2 = convert_pred_named env p2 in
    env, PLimplies(p1,p2)
  | Piff(p1,p2) ->
    let env, p1 = convert_pred_named env p1 in
    let env, p2 = convert_pred_named env p2 in
    env, PLiff(p1,p2)
  | Pnot p ->
    let env, p = convert_pred_named env p in
    env, PLnot p
  | Pif(cond,ptrue,pfalse) ->
    let env, cond = convert_logic_expr env cond in
    let env, ptrue = convert_pred_named env ptrue in
    let env, pfalse = convert_pred_named env pfalse in
    env, PLif(cond,ptrue,pfalse)
  | Plet(li,p) ->
    let env, body = convert_inner_body env li in
    let env, p = convert_pred_named env p in
    env, PLlet(li.li_name.decl_name,body,p)
  | Pforall (x,p) ->
    let env, quants = Convert_env.env_map convert_var_decl env x in
    let env, p = convert_pred_named env p in
    env, PLforall(quants,p)
  | Pexists(x,p) ->
    let env, quants = Convert_env.env_map convert_var_decl env x in
    let env, p = convert_pred_named env p in
    env, PLexists(quants,p)
  | Pat(l,p) ->
    let l = convert_logic_label l in
    let env, p = convert_pred_named env p in
    env, PLat(p,l)
  | Pvalid_function(t) ->
    let env, t = convert_logic_expr env t in
    env, PLvalid_function(t)
  | Pvalid_read(l,t) ->
    let l = convert_logic_label_opt l in
    let env, t = convert_logic_expr env t in
    env, PLvalid_read(l,t)
  | Pvalid(l,t) ->
    let l = convert_logic_label_opt l in
    let env, t = convert_logic_expr env t in
    env, PLvalid(l,t)
  | Pobject_pointer(l,t) ->
    let l = convert_logic_label_opt l in
    let env, t = convert_logic_expr env t in
    env, PLobject_pointer(l,t)
  | Pinitialized(l,t) ->
    let l = convert_logic_label_opt l in
    let env, t = convert_logic_expr env t in
    env, PLinitialized(l,t)
  | Pallocable(l,t) ->
    let l = convert_logic_label_opt l in
    let env, t = convert_logic_expr env t in
    env, PLallocable(l,t)
  | Pfreeable(l,t) ->
    let l = convert_logic_label_opt l in
    let env, t = convert_logic_expr env t in
    env, PLfreeable(l,t)
  | Pfresh(Some l1, Some l2,t1,t2) ->
    let l1 = convert_logic_label l1 in
    let l2 = convert_logic_label l2 in
    let env, t1 = convert_logic_expr env t1 in
    let env, t2 = convert_logic_expr env t2 in
    env, PLfresh (Some(l1,l2),t1,t2)
  | Pfresh(None,None,t1,t2) ->
    let env, t1 = convert_logic_expr env t1 in
    let env, t2 = convert_logic_expr env t2 in
    env, PLfresh (None,t1,t2)
  | Pfresh(_,_,_,_) -> Frama_Clang_option.fatal
      "zero or two labels needed in fresh construct"
  | Psubtype _ -> Frama_Clang_option.not_yet_implemented "subtyping relation"

and convert_inner_body env li =
  let env, body =
    match li.fun_body with
      | LBnone -> Convert_env.fatal env "local binding without body"
      | LBreads _ -> Convert_env.fatal env "local binding with read clause"
      | LBinductive _ -> Convert_env.fatal env "local inductive definition"
      | LBterm t -> convert_logic_expr env t
      | LBpred p -> convert_pred_named env p
  in
  match li.profile with
    | [] -> env, body
    | prms ->
      let convert_arg_decl env a =
        let env, typ = convert_logic_type env a.la_type in
        let v = a.la_name in
        env, (typ, v)
      in
      let env, prms = Convert_env.env_map convert_arg_decl env prms in
      env,
      lexpr env (PLlambda(prms,body))

let convert_logic_ctor env ctor =
  (* NB: we can't rely on ACSL overloading for constructor. Maybe we
     should get the whole signature? *)
  let name =
    if ctor.logic_ctor_info_is_extern_c then
      ctor.ctor_name.decl_name
    else
      let ctor_name, t
        = Convert_env.typedef_normalize env ctor.ctor_name TStandard in
      Mangling.mangle ctor_name t None
  in
  let env, prms =
    Convert_env.env_map convert_logic_type env ctor.ctor_params
  in
  env, (name,prms)

let convert_logic_type_def env = function
  | LTsum ctors ->
    let env, ctors = Convert_env.env_map convert_logic_ctor env ctors in
    env, TDsum ctors
  | LTsyn t ->
    let env, t = convert_logic_type env t in env, TDsyn t

let convert_extended env e =
  let env, preds = Convert_env.env_map convert_pred_named env e.predicates in
  env, Logic_ptree.{
    ext_name = e.ext_name;
    ext_plugin = e.ext_plugin;
    ext_content =  preds;
  }

let convert_allocation env = function
  | Intermediate_format.FreeAlloc(f,a) ->
    let env, f = Convert_env.env_map convert_logic_expr env f in
    let env, a = Convert_env.env_map convert_logic_expr env a in
    env, Logic_ptree.FreeAlloc (f,a)
  | Intermediate_format.FreeAllocAny -> env, Logic_ptree.FreeAllocAny

let convert_deps env = function
  | Intermediate_format.From l ->
    let env, l = Convert_env.env_map convert_logic_expr env l in
    env, Logic_ptree.From l
  | Intermediate_format.FromAny -> env, Logic_ptree.FromAny

let convert_from env f =
  let env, w = convert_logic_expr env f.floc in
  let env, r = convert_deps env f.vars in
  env, (w,r)

let convert_assigns env = function
  | Intermediate_format.WritesAny -> env, Logic_ptree.WritesAny
  | Intermediate_format.Writes l ->
    let env, l = Convert_env.env_map convert_from env l in
    env, Logic_ptree.Writes l

let convert_pred_tp env ?(kind=Cil_types.Assert) p =
  (* TODO: support check and admit in ACSL++. *)
  let env, tp_statement = convert_pred_named env p in
  env, { tp_kind = kind; tp_statement }

let convert_termination_kind = function
  | Intermediate_format.Normal -> Cil_types.Normal
  | Intermediate_format.Exits -> Cil_types.Exits
  | Intermediate_format.Breaks -> Cil_types.Breaks
  | Intermediate_format.Continues -> Cil_types.Continues
  | Intermediate_format.Returns -> Cil_types.Returns

let convert_post_cond env p =
  let kind = convert_termination_kind p.tkind in
  let env, p = convert_pred_tp env p.pred in
  env, (kind, p)

let convert_behavior env bhv =
  let b_name = bhv.beh_name in
  let env_map = Convert_env.env_map in
  (* optional arguments do not mix well with higher-order funcs *)
  let convert_pred_tp env p = convert_pred_tp env p in
  let env, b_requires = env_map convert_pred_tp env bhv.requires in
  let env, b_assumes = env_map convert_pred_named env bhv.assumes in
  let env, b_post_cond = env_map convert_post_cond env bhv.post_cond in
  let env, b_assigns = convert_assigns env bhv.assignements in
  let env, b_allocation = convert_allocation env bhv.alloc in
  let env, b_extended = env_map convert_extended env bhv.extended in
  env, { b_name; b_requires; b_assumes; b_post_cond;
    b_assigns; b_allocation; b_extended }

let convert_variant env v =
  let env, body = convert_logic_expr env v.vbody in
  env, (body, v.vname)

let convert_function_contract env contract =
  let env_map = Convert_env.env_map in
  let env_opt = Convert_env.env_opt in
  let env, spec_behavior = env_map convert_behavior env contract.behavior in
  let env, spec_variant = env_opt convert_variant env contract.variant in
  let env, spec_terminates =
    env_opt convert_pred_named env contract.terminates
  in
  let spec_complete_behaviors =
    List.map (fun c -> c.behaviors) contract.complete_behaviors
  in
  let spec_disjoint_behaviors =
    List.map (fun c -> c.behaviors) contract.disjoint_behaviors
  in
  env,
  { spec_behavior; spec_variant; spec_terminates;
    spec_complete_behaviors; spec_disjoint_behaviors }

let convert_inv_kind = function
  | InvariantAsAssertion -> false
  | NormalLoop -> true

let convert_code_annot env = function
  | Intermediate_format.Assert(bhvs,pred) ->
    let env, pred = convert_pred_tp env pred in
    env, AAssert(bhvs, pred)
  | StmtSpec(bhvs,spec) ->
    let env, spec = convert_function_contract env spec in
    env, AStmtSpec(bhvs, spec)
  | Invariant(bhvs,kind,inv) ->
    let kind = convert_inv_kind kind in
    let env, inv = convert_pred_tp env inv in
    env, AInvariant(bhvs,kind,inv)
  | Variant v ->
    let env, v = convert_variant env v in
    env, AVariant v
  | Assigns (bhvs,a) ->
    let env, a = convert_assigns env a in
    env, AAssigns(bhvs, a)
  | Allocation(bhvs,a) ->
    let env, a = convert_allocation env a in
    env, AAllocation(bhvs,a)

let rec convert_annot env annot =
  let open Current_loc.Operators in
  let env = Convert_env.set_loc env (find_loc_glob_annot annot) in
  let loc = Convert_env.get_loc env in
  let<> UpdatedCurrentLoc = loc in
  let env, annot =
    match annot with
      | Dfun_or_pred (_,info) ->
        (* we don't necessarily need to mangle according to the signature,
           as ACSL itself feature overloading. If we decide to have unique
           names, we'll need to adapt mangling to accomodate for logic types.
         *)
        let name =
          if info.li_extern_c then
            info.li_name.decl_name
          else
            let info_name, t
              = Convert_env.typedef_normalize env info.li_name TStandard in
            Mangling.mangle info_name t None
        in
        let labels = List.map convert_logic_label info.arg_labels in
        let env, rt =
          Convert_env.env_opt convert_logic_type env info.returned_type
        in
        let env, params =
          Convert_env.env_map convert_logic_param env info.profile
        in
        (match info.fun_body,rt with
          | LBnone, None ->
            env, LDpredicate_reads(name,labels,info.tparams,params,None)
          | LBnone, Some rt ->
            env, LDlogic_reads(name,labels,info.tparams,rt,params,None)
          | LBreads l, None ->
            let env, reads = Convert_env.env_map convert_logic_expr env l in
            env, LDpredicate_reads(name,labels,info.tparams,params,Some reads)
          | LBreads l, Some rt ->
            let env, reads = Convert_env.env_map convert_logic_expr env l in
            env, LDlogic_reads(name,labels,info.tparams,rt,params,Some reads)
          | LBterm _, None ->
            Convert_env.fatal env "predicate definition with a term as body"
          | LBterm body, Some rt ->
            let env, body = convert_logic_expr env body in
            env, LDlogic_def(name,labels,info.tparams,rt,params,body)
          | LBpred _, Some _ ->
            Convert_env.fatal env
              "logic function definition with a predicate as body"
          | LBpred body, None ->
            let env, body = convert_pred_named env body in
            env, LDpredicate_def(name,labels,info.tparams,params,body)
          | LBinductive _, Some _ ->
            Convert_env.fatal env
              "logic function definition with inductive body"
          | LBinductive body, None ->
            let inductive_case env ind =
              let labs = List.map convert_logic_label ind.labels in
              let env, def = convert_pred_named env ind.def in
              env, (ind.def_name,labs,ind.arguments,def)
            in
            let env, body = Convert_env.env_map inductive_case env body in
            env, LDinductive_def(name,labels,info.tparams,params,body)
        )
      | Dvolatile(_,mem,read,write) ->
        let env, mem = Convert_env.env_map convert_logic_expr env mem in
        (* NB: should we have the real type of the arguments of the functions?*)
        let mangle name signature =
          let name, t = Convert_env.typedef_normalize env name TStandard in
          Mangling.mangle name t signature in
        let read = Option.map (Fun.flip mangle None) read in
        let write = Option.map (Fun.flip mangle None) write in
        env, LDvolatile(mem,(read,write))
      | Daxiomatic(_,s,annots) ->
        let env, annots = Convert_env.env_map convert_annot env annots in
        env, LDaxiomatic(s,annots)
      | Dtype(_,lt_info) ->
        let name =
          if lt_info.logic_type_info_is_extern_c then
            lt_info.type_name.decl_name
          else
            let info_name, t =
              Convert_env.typedef_normalize env lt_info.type_name TStandard
            in
            Mangling.mangle info_name t None
        in
        let env, def =
          Convert_env.env_opt convert_logic_type_def env lt_info.definition
        in
        env, LDtype(name, lt_info.params, def)
      | Dlemma(_,name,is_axiom,labs,params,body) ->
        let labs = List.map convert_logic_label labs in
        let kind = if is_axiom then Cil_types.Admit else Assert in
        let env, body = convert_pred_tp env ~kind body in
        env, LDlemma(name,labs,params,body)
      | Dinvariant(_,body) ->
        let name =
          if body.li_extern_c then
            body.li_name.decl_name
          else
            let body_name, t = Convert_env.typedef_normalize
                  env body.li_name TStandard in
            Mangling.mangle body_name t None
        in
        let env, body =
          match body.fun_body with
            | LBpred p ->
              convert_pred_named env p
            | _ ->
              Convert_env.fatal env "unexpected body for a global invariant"
        in
        env, LDinvariant(name, body)
      | Dtype_annot (_, body) ->
        let inv_name =
          if body.li_extern_c then
            body.li_name.decl_name
          else
            let body_name, t =
              Convert_env.typedef_normalize env body.li_name TStandard
            in
            Mangling.mangle body_name t None
        in
        let env, (this_type, this_name) =
          match body.profile with
            | [ { la_type = t; la_name = s } ] ->
              let env, t = convert_logic_type env t in env, (t, s)
            | _ ->
              Convert_env.fatal env
                "unexpected number of parameters \
                 in definition of type invariant"
        in
        let env, inv =
          match body.fun_body with
            | LBpred p -> convert_pred_named env p
            | _ -> Convert_env.fatal env "unexpected body for a type invariant"
        in
        env, LDtype_annot { inv_name; this_type; this_name; inv }
      | Dmodel_annot(_,model) ->
        let model_name = model.Intermediate_format.model_name in
        let env, model_type = convert_logic_type env model.field_type in
        let env, model_for_type = convert_logic_type env model.base_type in
        env, LDmodel_annot { model_for_type; model_type; model_name }
  in
  env, { decl_node = annot; decl_loc = Convert_env.get_loc env; }
