# Version 0.0.17

- Compatibility with Frama-C 30 Zinc

# Version 0.0.16

- Better handling of ACSL constructions
- Compatibility with Clang 18
- Compatibility with Frama-C 29 Copper

# Version 0.0.15

- Better handling of mixed C/C++ code and `extern "C"` declarations
- Compatibility with Clang 17
- Compatibility with Frama-C 28.x Nickel

# Version <= 0.0.14

- See https://frama-c.com/fc-plugins/frama-clang.html
