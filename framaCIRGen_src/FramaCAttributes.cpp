/**************************************************************************/
/*                                                                        */
/*  This file is part of Frama-Clang                                      */
/*                                                                        */
/*  Copyright (C) 2012-2025                                               */
/*    CEA (Commissariat à l'énergie atomique et aux énergies              */
/*         alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

//
// Description:
//  Handling of particular Frama-C attributes
//

#include "clang/AST/ASTContext.h"
#include "clang/AST/Attr.h"
#include "clang/Basic/Version.h"
#include "clang/Sema/ParsedAttr.h"
#include "clang/Sema/Sema.h"
#include "clang/Sema/SemaDiagnostic.h"
#include "llvm/IR/Attributes.h"
#include "clang/Basic/Version.h"

using namespace clang;

namespace {

struct FramaCBuiltinAttrInfo : public ParsedAttrInfo {
  FramaCBuiltinAttrInfo() {
    OptArgs = 0;
    static constexpr Spelling S[] = {{ParsedAttr::AS_GNU, "FC_BUILTIN"},
                                     {ParsedAttr::AS_CXX11, "FC_BUILTIN"}};
    Spellings = S;
  }

  bool diagAppertainsToDecl(Sema &S, const ParsedAttr &Attr,
                            const Decl *D) const override {
    // This attribute appertains to functions only.
    if (!isa<FunctionDecl>(D)) {
      S.Diag(Attr.getLoc(), diag::warn_attribute_wrong_decl_type_str)
        << Attr << "functions";
      return false;
    }
    return true;
  }

  AttrHandling handleDeclAttribute(Sema &S, Decl *D,
                                   const ParsedAttr &Attr) const override {
    // Check if we have an optional string argument.
    if (Attr.getNumArgs() != 0) {
      Expr *ArgExpr = Attr.getArgAsExpr(0);
      S.Diag(ArgExpr->getExprLoc(), diag::err_attribute_argument_type)
        << Attr.getAttrName() << AANT_ArgumentString;
      return AttributeNotApplied;
    }
    // Attach an annotate attribute to the Decl.
#if CLANG_VERSION_MAJOR >= 17
    D->addAttr(AnnotateAttr::Create(S.Context, "FC_BUILTIN", Attr));
#else
    D->addAttr(AnnotateAttr::Create(S.Context, "FC_BUILTIN", Attr.getRange()));
#endif
    return AttributeApplied;
  }
};

} // namespace

static ParsedAttrInfoRegistry::Add<FramaCBuiltinAttrInfo> X("FC_BUILTIN", "");
