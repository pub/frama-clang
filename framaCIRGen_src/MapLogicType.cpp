/**************************************************************************/
/*                                                                        */
/*  This file is part of Frama-Clang                                      */
/*                                                                        */
/*  Copyright (C) 2012-2025                                               */
/*    CEA (Commissariat à l'énergie atomique et aux énergies              */
/*         alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

#include "ACSLLogicType.h"
#include "intermediate_format.h"
#include "MapLogicType.h"
#include <utility>
namespace Acsl {
  logic_type MapLogicType::unroll(
    logic_type type, Parser::Arguments& context) {
    while(type->tag_logic_type == LNAMED || type->tag_logic_type == LREFERENCE) {
      if (type->tag_logic_type == LREFERENCE) {
        type = type->cons_logic_type.Lreference.subtype;
        continue;
      }
      GlobalContext::LogicType* typeDefinition
        = context.findGlobalLogicType(type->cons_logic_type.Lnamed.name);
      if (typeDefinition && is_type_synonym(typeDefinition->type_info())) {
        /* string */ list formalParameters
          = typeDefinition->type_info()->params;
        /* logic_type */ list actualParameters
          = type->cons_logic_type.Lnamed.template_arguments;
        type =
          logic_type_dup(extract_synonym_def(typeDefinition->type_info()));
        std::map<std::string,logic_type> map;
        while (formalParameters) {
          if (actualParameters == NULL) {
            context.addErrorMessage(
              "Logic type used with wrong number of parameters");
            assert(false);
          };
          map.insert(
            std::make_pair(
              std::string((const char*) formalParameters->element.container),
              (logic_type) actualParameters->element.container));
          formalParameters = formalParameters->next;
          actualParameters = actualParameters->next;
        };
        if (actualParameters != NULL) {
          context.addErrorMessage(
            "Logic type used with wrong number of parameters");
          assert(false);
        };
        Acsl::instantiate(map,type);
      } else break;
    }
    return type;
  }

  logic_type MapLogicType::isCompatibleType(
    logic_type& lfirst, logic_type& lsecond,
    Parser::Arguments& context, bool strict) {

    if(!strict) {
      // in logic, references are mostly equivalent to plain types
      while(lfirst->tag_logic_type == LREFERENCE)
        lfirst = lfirst->cons_logic_type.Lreference.subtype;
      while(lsecond->tag_logic_type == LREFERENCE)
        lsecond = lsecond->cons_logic_type.Lreference.subtype;
      // set conversions if needed
      logic_type felt = Acsl::isSetType(lfirst);
      logic_type selt = Acsl::isSetType(lsecond);
      if (felt && selt) {
        if (logic_type set = isCompatibleType(felt,selt,context,strict))
          return make_set_type(set);
        else return NULL;
      }
      if (felt) {
        if (logic_type set = isCompatibleType(felt,lsecond,context,strict))
          return make_set_type(set);
        else return NULL;
      }
      if (selt) {
        if (logic_type set = isCompatibleType(lfirst,selt,context,strict))
          return make_set_type(set);
        else return NULL;
      }
      // Coercions
      if (lfirst->tag_logic_type == LREAL && isArithmeticType(lsecond,context))
        return lfirst;
      if (lfirst->tag_logic_type == LINTEGER && isIntegralType(lsecond,context))
        return lfirst;
       if (isArithmeticType(lfirst,context) && lsecond->tag_logic_type == LREAL)
        return lsecond;
      if (lfirst->tag_logic_type == LBOOLEAN && lsecond->tag_logic_type == LBOOLEAN)
        return lfirst;
      if (isIntegralType(lfirst,context) && lsecond->tag_logic_type == LINTEGER)
        return lsecond;
     // function and function pointer are compatibles
      if (lfirst->tag_logic_type == LARROW && lsecond->tag_logic_type == LPOINTER) {
        if (isCompatibleType(
              lfirst,lsecond->cons_logic_type.Lpointer.subtype,context))
          return lsecond; // prefer the pointer
        else return NULL;
      }
      if (lfirst->tag_logic_type == LPOINTER && lsecond->tag_logic_type == LARROW) {
        if (isCompatibleType(
              lfirst->cons_logic_type.Lpointer.subtype,lsecond,context))
          return lfirst; // prefer the pointer
        else return NULL;
      }
    }

    if (lfirst->tag_logic_type == LVARIABLE) {
      if (lsecond->tag_logic_type == LVARIABLE &&
          strcmp(lfirst->cons_logic_type.Lvariable.name,
                 lsecond->cons_logic_type.Lvariable.name) == 0) {
        // exact same type, no need to perform instantiations
        return lfirst;
      }
      std::string fvar(lfirst->cons_logic_type.Lvariable.name);
      iterator ftype = this->find(fvar);
      if (ftype != end()) {
        if (ftype->second != NULL) {
          instantiate(ftype->second);
          free_logic_type(lfirst);
          lfirst = logic_type_dup(ftype->second);
          return isCompatibleType(lfirst,lsecond,context,strict);
        }
        // no binding yet. Use lsecond, unless it is itself a free variable, in
        // which case we use lexicographic order to choose which one gets bound
        if (lsecond->tag_logic_type == LVARIABLE) {
          std::string svar(lsecond->cons_logic_type.Lvariable.name);
          iterator stype = this->find(svar);
          if (stype != end()) {
            logic_type inst = stype->second;
            if (inst != NULL) {
              instantiate(inst);
              if (inst->tag_logic_type == LVARIABLE) {
                std::string ssvar(inst->cons_logic_type.Lvariable.name);
                if (this->find(ssvar)!=end() && fvar < ssvar) {
                  this->insert(
                    std::make_pair(ssvar,logic_type_Lvariable(fvar.c_str())));
                  this->insert(
                    std::make_pair(svar,logic_type_Lvariable(fvar.c_str())));
                  return lfirst;
                }
                // in all other cases, we choose inst to represent fvar as well.
              }
              this->insert(std::make_pair(fvar,inst));
              free_logic_type(lfirst);
              lfirst = logic_type_dup(inst);
              return lfirst;
            }
            // we have two free variables. choose one to represent the other.
            if (fvar < svar) {
              this->insert(
                std::make_pair(svar,logic_type_Lvariable(fvar.c_str())));
              free_logic_type(lsecond);
              lsecond = logic_type_Lvariable(fvar.c_str());
            } else {
              this->insert(
                std::make_pair(fvar,logic_type_Lvariable(svar.c_str())));
              free_logic_type(lfirst);
              lfirst = logic_type_Lvariable(svar.c_str());
            }
            return lfirst;
          }
          // lsecond is a non-free variable (bound to the list of polymorphic
          // variables in the underlying global logic declaration), we just use
          // that for the instance.
          this->insert(std::make_pair(fvar, logic_type_dup(lsecond)));
          free_logic_type(lfirst);
          lfirst=logic_type_dup(lsecond);
          return lfirst;
        }
      }
      // lfirst is a non-free variable, what about lsecond?
      if (lsecond->tag_logic_type == LVARIABLE) {
        std::string svar(lsecond->cons_logic_type.Lvariable.name);
        if (svar == fvar) return lfirst;
        iterator stype = this->find(svar);
        if (stype == end()) return NULL; // two distinct non-free variables
        if (stype->second == NULL) {
          this->insert(std::make_pair(svar, logic_type_dup(lfirst)));
          free_logic_type(lsecond);
          lsecond=logic_type_dup(lfirst);
          return lfirst;
        }
        logic_type inst = stype->second;
        instantiate(inst);
        if (inst->tag_logic_type != LVARIABLE) return NULL;
        std::string ssvar(inst->cons_logic_type.Lvariable.name);
        if (ssvar == fvar) return lfirst;
        if (this->find(ssvar)==end()) return NULL;
        this->insert(std::make_pair(ssvar, logic_type_dup(lfirst)));
        free_logic_type(lsecond);
        lsecond = logic_type_dup(lfirst);
        return lfirst;
      }
    }
    // lfirst is not a variable. Check for lsecond
    if (lsecond->tag_logic_type == LVARIABLE) {
      std::string svar(lsecond->cons_logic_type.Lvariable.name);
      iterator stype = this->find(svar);
      if (stype == end()) {
        // No binding yet, use lfirst
        this->insert(std::make_pair(svar,logic_type_dup(lfirst)));
        free_logic_type(lsecond);
        lsecond = logic_type_dup(lfirst);
        return lfirst;
      }
      logic_type inst = stype->second;
      instantiate(inst);
      if (inst->tag_logic_type == LVARIABLE) {
        std::string ssvar(inst->cons_logic_type.Lvariable.name);
        if (this->find(ssvar) == end()) return NULL;
        this->insert(std::make_pair(ssvar,logic_type_dup(lfirst)));
        free_logic_type(lsecond);
        lsecond = logic_type_dup(lfirst);
        return lfirst;
      }
      return isCompatibleType(lfirst,inst,context,strict);
    }
    // neither lfirst nor lsecond are variable. Check for type synonyms
    if (lfirst->tag_logic_type == LNAMED || lsecond->tag_logic_type == LNAMED) {
      if (lfirst->tag_logic_type == lsecond->tag_logic_type &&
          qualified_name_equal(
            lfirst->cons_logic_type.Lnamed.name,
            lsecond->cons_logic_type.Lnamed.name)) {
        list fprm = lfirst->cons_logic_type.Lnamed.template_arguments;
        list sprm = lsecond->cons_logic_type.Lnamed.template_arguments;
        if (isCompatibleTypeList(fprm,sprm,context,strict)) return lfirst;
        else return NULL;
      }
      // Check for equality modulo typedefs
      if (lfirst->tag_logic_type == LNAMED) {
        logic_type def = unroll(lfirst,context);
        if (def != lfirst) {
          free_logic_type(lfirst);
          lfirst = def;
          return isCompatibleType(def,lsecond,context,strict);
        }
      }
      if (lsecond->tag_logic_type == LNAMED) {
        logic_type def = unroll(lsecond,context);
        if (def != lsecond) {
          free_logic_type(lsecond);
          lsecond = def;
          return isCompatibleType(lfirst,def,context,strict);
        }
      }
      return NULL;
    }
    if(lfirst->tag_logic_type == LCNAMED) {
      logic_type unroll =
        context.makeLogicType(lfirst->cons_logic_type.LCnamed.name);
      return isCompatibleType(unroll,lsecond,context,strict);
    }
    if(lsecond->tag_logic_type == LCNAMED) {
      logic_type unroll=
        context.makeLogicType(lsecond->cons_logic_type.LCnamed.name);
      return isCompatibleType(lfirst,unroll,context,strict);
    }
    if (lfirst->tag_logic_type != lsecond->tag_logic_type) return NULL;
    switch (lsecond->tag_logic_type) {
      case LVOID:
      case LINTEGER:
      case LREAL:
        return lfirst;
      case LINT:
        return
          equivalent_int_kind(
            lfirst->cons_logic_type.Lint.kind,
            lsecond->cons_logic_type.Lint.kind,
            context)
          ?lfirst:NULL;
      case LFLOAT:
        return
          lfirst->cons_logic_type.Lfloat.kind
          == lsecond->cons_logic_type.Lfloat.kind
          ?lfirst:NULL;
      case LARRAY:
        if (!isCompatibleType(
              lfirst->cons_logic_type.Larray.subtype,
              lsecond->cons_logic_type.Larray.subtype,
              context,strict))
          return NULL;
        if (lfirst->cons_logic_type.Larray.dim->is_some) {
          if (lsecond->cons_logic_type.Larray.dim->is_some)
            return logic_constant_equal(
              (logic_constant) lfirst->cons_logic_type.Larray
              .dim->content.container,
              (logic_constant) lsecond->cons_logic_type.Larray
              .dim->content.container)?lfirst:NULL;
          return NULL;
        }
        return !lsecond->cons_logic_type.Larray.dim->is_some?lfirst:NULL;
      case LPOINTER: {
        logic_type typ =
          isCompatibleType(
            lfirst->cons_logic_type.Lpointer.subtype,
            lsecond->cons_logic_type.Lpointer.subtype,
            context,strict);
        if (typ) return lfirst;
        if (isLogicVoidPointerType(lfirst,context)) return lsecond;
        if (isLogicVoidPointerType(lsecond,context)) return lfirst;
        return NULL;
      }
      case LENUM:
        return
          qualified_name_equal(
            lfirst->cons_logic_type.Lenum.name,
            lsecond->cons_logic_type.Lenum.name)
          ?lfirst:NULL;
      case LSTRUCT:
        return
          qualified_name_equal(
            lfirst->cons_logic_type.Lstruct.name,
            lsecond->cons_logic_type.Lstruct.name)
          &&
          tkind_equal(
            lfirst->cons_logic_type.Lstruct.template_kind,
            lsecond->cons_logic_type.Lstruct.template_kind)
          ?lfirst:NULL;
      case LUNION:
        return
          qualified_name_equal(
            lfirst->cons_logic_type.Lunion.name,
            lsecond->cons_logic_type.Lunion.name)
          &&
          tkind_equal(
            lfirst->cons_logic_type.Lunion.template_kind,
            lsecond->cons_logic_type.Lunion.template_kind)
          ?lfirst:NULL;
      case LNAMED: {
        if (!qualified_name_equal(lfirst->cons_logic_type.Lnamed.name,
                                  lsecond->cons_logic_type.Lnamed.name))
          return NULL;
        list firstCursor = lfirst->cons_logic_type.Lnamed.template_arguments;
        list secondCursor = lsecond->cons_logic_type.Lnamed.template_arguments;
        if (isCompatibleTypeList(firstCursor,secondCursor,context,strict))
          return lfirst;
        else return NULL;
      };
      case LARROW:
        { /* logic_type */ list firstCursor = lfirst->cons_logic_type.Larrow.left;
          /* logic_type */ list secondCursor =
            lsecond->cons_logic_type.Larrow.left;
          if (isCompatibleTypeList(firstCursor,secondCursor,context,strict) &&
              isCompatibleType(
                lfirst->cons_logic_type.Larrow.right,
                lsecond->cons_logic_type.Larrow.right,
                context,strict))
            return lfirst;
          else return NULL;
        };
      default:
        return NULL;
    }
  }

  bool MapLogicType::isCompatibleTypeList(
    list lfirst, list lsecond, Parser::Arguments&context, bool strict) {
    while (lfirst != NULL) {
      if (lsecond == NULL) return false;
      logic_type farg = (logic_type)lfirst->element.container;
      logic_type sarg = (logic_type)lsecond->element.container;
      if (!isCompatibleType(farg,sarg,context,strict)) return false;
      lfirst->element.container = farg;
      lsecond->element.container = sarg;
      lfirst = lfirst->next;
      lsecond = lsecond->next;
    };
    return lsecond == NULL;
  }

bool
MapLogicType::isIntegralType(logic_type ctype, Parser::Arguments& context) {
  ctype = elt_or_singleton(unroll(ctype,context));
  bool result = (ctype->tag_logic_type == LINTEGER)
      || (ctype->tag_logic_type == LINT)
      || (ctype->tag_logic_type == LENUM);
  if (!result && ctype->tag_logic_type == LCNAMED)
    result = context.isIntegralTypedefType(ctype->cons_logic_type.LCnamed.name);
  if (!result)
    result = (ctype->tag_logic_type == LBOOLEAN);
  return result;
}

bool
MapLogicType::isArithmeticType(logic_type ctype, Parser::Arguments& context) {
  ctype = elt_or_singleton(unroll(ctype,context));
  if (ctype->tag_logic_type == LREFERENCE)
    ctype = ctype->cons_logic_type.Lreference.subtype;
  bool result = (ctype->tag_logic_type == LINT)
    || (ctype->tag_logic_type == LENUM)
    || (ctype->tag_logic_type == LFLOAT)
    || (ctype->tag_logic_type == LINTEGER)
    || (ctype->tag_logic_type == LREAL)
    ;
  if (!result && ctype->tag_logic_type == LCNAMED)
    result = context.isArithmeticTypedefType(ctype->cons_logic_type
        .LCnamed.name);
  return result;
}

  bool
  MapLogicType::isCPointerType(logic_type ltype, Parser::Arguments& context) {
    ltype = elt_or_singleton(unroll(ltype,context));
    tag_logic_type tagType = ltype->tag_logic_type;
    return tagType == LPOINTER ||
      (tagType == LCNAMED
       && context.isPointerTypedefType(ltype->cons_logic_type.LCnamed.name));
  }

  bool
  MapLogicType::isCReferenceType(logic_type ltype, Parser::Arguments& context)
  {
    ltype = elt_or_singleton(unroll(ltype,context));
    tag_logic_type tagType = ltype->tag_logic_type;
    return tagType == LREFERENCE ||
      (tagType == LCNAMED
       && context.isReferenceTypedefType(ltype->cons_logic_type.LCnamed.name));
  }

  bool
  MapLogicType::isCArrayType(logic_type ltype, Parser::Arguments& context) {
    ltype = elt_or_singleton(unroll(ltype,context));
    tag_logic_type tagType = ltype->tag_logic_type;
    return tagType == LARRAY ||
      (tagType == LCNAMED
       && context.isArrayTypedefType(ltype->cons_logic_type.LCnamed.name));
  }

  bool MapLogicType::isSetType(logic_type ltype, Parser::Arguments& context) {
    instantiate(ltype);
    ltype = unroll(ltype,context);
    return Acsl::isSetType(ltype) != NULL;
  }

  logic_type MapLogicType::typeOfPointed(
    logic_type ltype, Parser::Arguments& context) {
    ltype = elt_or_singleton(unroll(ltype,context));
    switch (ltype->tag_logic_type) {
      case LPOINTER:
        return logic_type_dup(ltype->cons_logic_type.Lpointer.subtype);
      case LCNAMED:
        return context.makeTypeOfPointed(ltype->cons_logic_type.LCnamed.name);
      default:
        context.addErrorMessage(
          "typeOfPointed expects a pointer type as argument");
        return NULL;
    }
  }

  logic_type MapLogicType::typeOfArrayElement(
    logic_type ltype, Parser::Arguments& context) {
    ltype = elt_or_singleton(unroll(ltype,context));
    switch (ltype->tag_logic_type) {
      case LARRAY:
        return logic_type_dup(ltype->cons_logic_type.Larray.subtype);
      case LCNAMED:
        return
          context.makeTypeOfArrayElement(ltype->cons_logic_type.LCnamed.name);
      default:
        context.addErrorMessage(
          "typeOfArrayElement expects an array type as argument");
        return NULL;
    }
  }

  logic_type MapLogicType::logicArithmeticConversion(
    logic_type t1, logic_type t2,Parser::Arguments& context) {
    t1 = unroll(t1,context);
    t2 = unroll(t2,context);
    if ((t1->tag_logic_type >= LINT && t1->tag_logic_type <= LCNAMED)
        && (t2->tag_logic_type >= LINT && t2->tag_logic_type <= LCNAMED)) {
      if (isIntegralType(t1, context) && isIntegralType(t2, context))
        return logic_type_Linteger();
      return logic_type_Lreal();
    };
    if (t1->tag_logic_type >= LINT && t1->tag_logic_type <= LCNAMED) {
      if (t2->tag_logic_type == LINTEGER) {
        if (isIntegralType(t1, context))
          return logic_type_Linteger();
        if (isArithmeticType(t1, context))
          return logic_type_Lreal();
      };
      if (t2->tag_logic_type == LREAL) {
        if (isArithmeticType(t1, context))
          return logic_type_Lreal();
      };
    };
    if (t2->tag_logic_type >= LINT && t2->tag_logic_type <= LCNAMED) {
      if (t1->tag_logic_type == LINTEGER) {
        if (isIntegralType(t2, context))
          return logic_type_Linteger();
        if (isArithmeticType(t2, context))
          return logic_type_Lreal();
      };
      if (t1->tag_logic_type == LREAL) {
        if (isArithmeticType(t2, context))
          return logic_type_Lreal();
      };
    };
    if (t1->tag_logic_type == LINTEGER && t2->tag_logic_type == LINTEGER)
      return logic_type_Linteger();
    if ((t1->tag_logic_type == LINTEGER || t1->tag_logic_type == LREAL)
        && (t2->tag_logic_type == LINTEGER || t2->tag_logic_type == LREAL))
      return logic_type_Lreal();
    if (Acsl::isSetType(t1) && !Acsl::isSetType(t2))
      return
        make_set_type(logicArithmeticConversion(remove_set_type(t1),t2,context));
    context.addErrorMessage("arithmetic conversion "
                            "between non arithmetic types!");
    return NULL;
  }

  bool MapLogicType::isLogicVoidPointerType(
    logic_type ltype, Parser::Arguments& context) {
    instantiate(ltype);
    ltype = unroll(ltype,context);
    return
      ltype->tag_logic_type == LPOINTER &&
      ltype->cons_logic_type.Lpointer.subtype->tag_logic_type == LVOID;
  }

}
