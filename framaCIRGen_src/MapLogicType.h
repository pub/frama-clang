/**************************************************************************/
/*                                                                        */
/*  This file is part of Frama-Clang                                      */
/*                                                                        */
/*  Copyright (C) 2012-2025                                               */
/*    CEA (Commissariat à l'énergie atomique et aux énergies              */
/*         alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/// Table of instantiations to deal with polymorphic types
/// when typechecking an ACSL term or predicate
#ifndef MAPLOGICTYPE_H
#define MAPLOGICTYPE_H

#include<map>
#include<string>

extern "C" {
#include "intermediate_format.h"
}

#include "ACSLLogicType.h"
#include "ACSLParser.h"

namespace Acsl {

  /*!
    table mapping free type variables to their current instantiation
    All free variable must have an entry in the table (potentially
    mapping to NULL), to distinguish them from the type variables that
    are bound within the global predicate/logic function declaration
    currently typechecked and thus cannot be instantiated (otherwise
    the type of the global symbol would be less general than what is
    declared).
   */
  class MapLogicType: std::map<std::string,logic_type> {
  private:
    unsigned long long _var_counter;

    bool isCompatibleTypeList(
      list lfirst, list lsecond, Parser::Arguments&context,bool strict);

  public:
  MapLogicType(): std::map<std::string,logic_type>(), _var_counter(0ULL) { }

    ~MapLogicType() {
      clear();
    }

    void instantiate(logic_type& type) {
      Acsl::instantiate(*this,type);
    }

    logic_type unroll(logic_type type, Parser::Arguments&context);

    logic_type fresh_type_variable(std::string v) {
      std::ostringstream str;
      str << v << "_" << _var_counter++;
      std::string var = str.str();
      this->insert(std::make_pair(var,(logic_type)NULL));
      return logic_type_Lvariable(var.c_str());
    }

    logic_type generic_pointer_type() {
      logic_type var = fresh_type_variable("ptr");
      logic_type ptr = logic_type_Lpointer(var);
      return ptr;
    }

    /*! Checks whether lsecond is compatible with lfirst, modulo instantiation
      of some type variables if needed.
      returns instantiated first type if both are compatible, NULL otherwise.
      If strict is true, standard implicit conversion (i.e. t to set<t>,
      C integer type to integer, C floating type to real) will not be done on
      the second type to check for compatibility.
    */
    logic_type isCompatibleType(
      logic_type& lfirst, logic_type& lsecond,
      Parser::Arguments& context, bool strict=false);

    bool isArithmeticType(logic_type ctype, Parser::Arguments& context);
    bool isIntegralType(logic_type ctype, Parser::Arguments& context);
    bool isCPointerType(logic_type ltype, Parser::Arguments& context);
    bool isCReferenceType(logic_type ltype, Parser::Arguments& context);
    bool isCArrayType(logic_type ltype, Parser::Arguments& context);
    bool isSetType(logic_type ltype, Parser::Arguments& context);
    bool isLogicVoidPointerType(logic_type ltype, Parser::Arguments& context);
    logic_type typeOfPointed(logic_type typ, Parser::Arguments& context);
    logic_type typeOfArrayElement(logic_type ltype, Parser::Arguments& context);
    logic_type logicArithmeticConversion(
      logic_type t1, logic_type t2,Parser::Arguments& context);
  };
};

#endif
