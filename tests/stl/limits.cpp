#include <limits>

void f() {
  float inf = std::numeric_limits<float>::infinity();
  double nan = std::numeric_limits<double>::quiet_NaN();
}
