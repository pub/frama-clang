/* run.config
   OPT: @EVA@ -main f @MACHDEP@ @CXX@
*/

#include <ratio>

using std::ratio;

void f() {
  constexpr int x = std::ratio_add<ratio<1,3>,ratio<1,2>>::num;
  static_assert( x == 5, "ratio conversion KO");
  constexpr int y = std::ratio_multiply<ratio<2,3>,ratio<3,4>>::num;
  static_assert( y == 1, "ratio conversion KO");
  constexpr int z = std::ratio_divide<ratio<2,9>,ratio<2,3>>:: num;
  static_assert ( z == 1, "ratio conversion KO");
  constexpr int t = std::ratio_subtract<ratio<1,3>,ratio<1,2>>::num; // should be -1;
  static_assert(t==-1,"ratio conversion KO");
  constexpr bool ok = std::ratio_equal<ratio<2,6>,ratio<1,3>>::value;
  constexpr bool ko = std::ratio_less<ratio<6,9>, ratio<3,6>>::value;
  static_assert(ok && !ko, "ratio comparison KO");

}
