/* run.config
   DEPS: static_1.hpp
   OPT: @MACHDEP@ %{dep:@PTEST_DIR@/static_1_aux.cpp} -print -check @CXX@
*/


// clang++ -Weverything init.cpp sm.cpp && ./a.out
// tis-analyzer-gui --no-libc -64 -val -cxx-keep-mangling init.cpp sm.cpp

#include "static_1.hpp"

extern "C" int printf(const char *, ...);

static int x;

int main(void)
{
    x = 43;
    printf("MARK %d %d\n", Sm_get(), Sm::cache);
    return 0;
}
