/* Generated by Frama-C */
struct _frama_c_vmt_content {
   void (*method_ptr)() ;
   int shift_this ;
};
struct _frama_c_rtti_name_info_node;
struct _frama_c_vmt {
   struct _frama_c_vmt_content *table ;
   int table_size ;
   struct _frama_c_rtti_name_info_node *rtti_info ;
};
struct _frama_c_rtti_name_info_content {
   struct _frama_c_rtti_name_info_node *value ;
   int shift_object ;
   int shift_vmt ;
};
struct _frama_c_rtti_name_info_node {
   char const *name ;
   struct _frama_c_rtti_name_info_content *base_classes ;
   int number_of_base_classes ;
   struct _frama_c_vmt *pvmt ;
};
struct B;
struct B {
   int x ;
};
struct _frama_c_rtti_name_info_node _frama_c_rtti_name_info;

/*@ requires \valid_read(this); */
void B::Ctor(struct B const *this)
{
  this->x = 42;
  return;
}

struct _frama_c_rtti_name_info_node _frama_c_rtti_name_info =
  {.name = "B",
   .base_classes = (struct _frama_c_rtti_name_info_content *)0,
   .number_of_base_classes = 0,
   .pvmt = (struct _frama_c_vmt *)0};
int main(void)
{
  int __retres;
  struct B b;
  B::Ctor((struct B const *)(& b));
  __retres = b.x;
  return __retres;
}


