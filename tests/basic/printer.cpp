/* run.config
NOFRAMAC:
EXECNOW: LOG printer.res.c @frama-c@ @PTEST_FILE@ @CXX@ @MACHDEP@ -cxx-parseable-output -ocode @PTEST_RESULT@/printer.res.c -print
EXECNOW: LOG printer.res2.c @frama-c@ @CXX@ @MACHDEP@ -cxx-unmangling without-qualifier %{dep:@PTEST_RESULT@/printer.res.c} -ocode @PTEST_RESULT@/printer.res2.c -print
*/

namespace A {
  struct B {
    int x;
    B() { x = 42; }
  };
}; // namespace A

int main() {
  A::B b;
  return b.x;
}
