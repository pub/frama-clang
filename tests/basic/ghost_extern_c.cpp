/* run.config
   OPT: -cxx-unmangling=none -print
*/
extern "C" {
  /*@ ghost int ghost_x; */
  int real_x;

  /*@ lemma sync: ghost_x == real_x; */
}
