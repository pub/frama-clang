struct A {
     int& a;
     int b;
};

typedef char myArray[10];

typedef struct myStruct {
   myArray a;
 } myStruct;

myStruct s = {};

const int I = -1;
const long L = -1L;
const long long LL = -1LL;

int main() {
     int x = -1;
     long long ll = I + L + LL;
     A y = { x, 1 };
     y.a++;
     return x;
}
