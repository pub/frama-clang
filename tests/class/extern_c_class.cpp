extern "C"{
  class C {
    void set(int* t) { *t = get_x(); foo(); }
    int get_x() { return 0; }
    virtual void foo();
  };
}
