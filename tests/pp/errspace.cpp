// Tests whether error PP directive with white space is properly reported
/*@

requires  \true;
	#  error "Failure"

*/
void m() {}
