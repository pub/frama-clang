/* run.config
DONTRUN: main test is the .cpp file
*/

//@ predicate test1(double x) = \is_finite(x) && 0.0 < x && x < 2.0;

//@ predicate test2(double x) = \is_finite(x) && 0.0 < x < 2.0;

//@ predicate test3(double x) = (\is_finite(x) && 0.0 < x) && x < 2.0;

//@ predicate test4(double x) = \is_finite(x) && (0.0 < x && x < 2.0);

//@ predicate test5(double x) = (\is_finite(x) && 0.0 < x) && (x < 2.0 && \is_finite(x));
