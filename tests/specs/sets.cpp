//@ lemma empty: \forall integer x; ! (x \in \empty);

//@ lemma non_empty: \forall integer x; x \in { x, 0, 1 };

//@ lemma non_empty_2: \forall int x; x \in { 0, 1 } || x < 0 || x > 1;
