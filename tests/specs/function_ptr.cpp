typedef void (*fct)(void);

extern "C" void f3();
void f4() { }
extern "C" void shadowed();

class A {
public:
  fct f;
  static void f1(void) { };
  static void f2(void) { };
  static void shadowed() { };
  /*@ predicate is_valid() = f == f1 || f == f2 || f == f3 ||
                             f == f4 || f == shadowed; */
  A(): f(f1) { };
};

A a;
